Notes on Optocouplers: 
 - LTV-847S, 50% min CTR. 
 
The output of the OC has two functions: 
	- Drive the input of the IO expander. 
	- Drive an indicator LED. 

Max input voltage: 24 V
Min input voltage: 3 V

With a minimum of 3V input voltage, 0.8 V forward voltage for the protection diode, 
and a 2.0V zener diode we are left with a voltage span of: 
3 - 0.8 - 2.0 = 0.2 V to 
24 - 0.8 - 2.0 = 21.2 V

Calculating the current assuming the minimum Vi: 
R = U/I, R = 0.2/0.0002 = 4500 Ohm.

In the case of the maximum input voltage, it is the power dissipation in the 
resistor that is governing: 
 I = 19.9 / 4700 = 0.044 A
 P = 19.9V * 0.0044 = 0.08 W
 
Choosing a resistor of 470 Ohms as a close value and redoing calculations: 
 I = 19.9 / 4.7K = 0.004 A
 P = 19.9 * 0004 = 0.08 W
 
 
 
 
Calculations on LED current: 
Assuming 1 mA minimum current on 

Inputs side: 
Vf = 1.4 max
I = 200 uA
Vz = 2V
Vr = 0.6V
R = 0.6/0.0002 = 3k

Using 2.7K: 
I = 0.6/2700 = 222 uA



TODO: 
- Perform ERC
- Add TVS diodes




Tests of inut stage:
 - Minimum voltage: 2.8 V
 - Maximum voltage: 24V. 2K resistor reaches 80 deg c in open air. 
 - Absolute maximum: 28V. 2K resistor reaches 100 deg c in open air.
 
 

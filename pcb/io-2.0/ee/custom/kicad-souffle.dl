#include "kicad.dl"

// Symbol libraries that indicate basic components
resistor_lib("IPC-7351-RESC").
capacitor_lib("IPC-7351-CAPC").

// Symbols that should be classified as net ties
net_tie_symbol("unlockoslo", "Net-Tie_3_to_1").

// Mappings from kicad's symbols to common footprint
.decl kicad_symbol_to_common(library:symbol, name:symbol, common:symbol)

add_fact(Part, "http://purl.org/ee/fact-type/footprint", common) :-
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#symbol-library", library),
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#symbol-name", name),
    kicad_symbol_to_common(library, name, common).

kicad_symbol_to_common("Isolator", "LTV-847S", "SMDIP-16_W9.53mm").
kicad_symbol_to_common("Interface_Expansion", "MCP23S17_ML", "QFN-28-1EP_6x6mm_P0.65mm_EP4.25x4.25mm").

// Mappings from kicad's footprints to common footprint
.decl kicad_footprint_to_common(library:symbol, footprint:symbol, common:symbol)

add_fact(Part, "http://purl.org/ee/fact-type/footprint", common) :-
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#footprint-library", library),
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#footprint-name", footprint),
    kicad_footprint_to_common(library, footprint, common).

kicad_footprint_to_common("Connector_PinHeader_2.54mm", "PinHeader_1x02_P2.54mm_Vertical", "PinHeader_1x02_P2.54mm_Vertical").
kicad_footprint_to_common("Connector_PinHeader_2.54mm", "PinHeader_1x03_P2.54mm_Vertical", "PinHeader_1x03_P2.54mm_Vertical").
kicad_footprint_to_common("Connector_PinHeader_2.54mm", "PinHeader_1x20_P2.54mm_Horizontal", "PinHeader_1x20_P2.54mm_Horizontal").
kicad_footprint_to_common("Diode_SMD", "D_SMA", "SMA").
kicad_footprint_to_common("Diode_SMD", "D_SOD-323", "SOD-323").
kicad_footprint_to_common("Diode_SMD", "D_SOT-23_ANK", "SOT-23-3").
kicad_footprint_to_common("Inductor_SMD", "L_Taiyo-Yuden_NR-80xx", "Taiyo-Yuden_NR-80xx").
kicad_footprint_to_common("Package_SO", "PowerIntegrations_SO-8", "SO-8 Single").
kicad_footprint_to_common("Package_TO_SOT_SMD", "SOT-23-6", "SOT-23-6").
kicad_footprint_to_common("Package_TO_SOT_SMD", "SOT-23", "SOT-23-3").
kicad_footprint_to_common("Package_TO_SOT_SMD", "TO-269AA", "TO-269AA").
kicad_footprint_to_common("TerminalBlock_Phoenix", "TerminalBlock_Phoenix_PT-1,5-2-3.5-H_1x02_P3.50mm_Horizontal", "TerminalBlock_Phoenix_PT-1,5-2-3.5-H_1x02_P3.50mm_Horizontal").
kicad_footprint_to_common("TerminalBlock_Phoenix", "TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal", "TerminalBlock_Phoenix_PT-1,5-3-3.5-H_1x03_P3.50mm_Horizontal").
kicad_footprint_to_common("TerminalBlock_Phoenix", "TerminalBlock_Phoenix_PT-1,5-4-3.5-H_1x04_P3.50mm_Horizontal", "TerminalBlock_Phoenix_PT-1,5-4-3.5-H_1x04_P3.50mm_Horizontal").
kicad_footprint_to_common("UnlockOslo", "1462042-8", "1462042-8").

.decl kicad_to_ipc(metric:symbol, kicad_footprint:symbol)
.output kicad_to_ipc

kicad_to_ipc(kicad, res) :-
    fact(_, "http://purl.org/ee/kicad-sch-fact-type#footprint-name", kicad),
    (match("RESC_....x.*", kicad);match("CAPC_....x.*", kicad)),
    res_ = substr(kicad, 5, strlen(kicad)-5),
    res = substr(res_, 0, strlen(res_)-4).

kicad_to_ipc(kicad, res) :-
    fact(_, "http://purl.org/ee/kicad-sch-fact-type#footprint-name", kicad),
    match("LEDSC_....x.*", kicad),
    res_ = substr(kicad, 6, strlen(kicad)-6),
    res = substr(res_, 0, strlen(res_)-4).

kicad_to_ipc("L_0805_2012Metric", "2012").
kicad_to_ipc("L_1206_3216Metric", "3216").

add_fact(Part, "http://purl.org/ee/fact-type/footprint", ipc) :-
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#footprint-name", footprint),
    kicad_to_ipc(footprint, ipc).

add_fact(Part, "http://purl.org/ee/fact-type/footprint", "smd capacitor d=8cm") :-
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#footprint", footprint),
    (footprint="Capacitor_SMD:CP_Elec_8x10.2").

add_fact(Part, "http://purl.org/ee/fact-type/footprint", "smd capacitor d=5cm") :-
    fact(Part, "http://purl.org/ee/kicad-sch-fact-type#footprint", footprint),
    (footprint="Capacitor_SMD:CP_Elec_5x5.4").

.decl skip_from_bom(ref:symbol)

// Parts from the schematic that is not really a part of the BOM.
skip_from_bom(Part) :-
    net_tie(Part);
    test_point(Part);
    mounting_hole(Part);
    fact(Part, "http://purl.org/ee/kicad-sch-custom-fact-type#populate", "DNP").

add_fact(Part, "http://purl.org/ee/fact-type/include-in-bom", "no") :- skip_from_bom(Part).
// add_fact(Part, "http://purl.org/ee/fact-type/schematic-only", "no") :- part(Part, _), !skip_from_bom(Part).

Optocoupler input protection. I found these discussions on the subject: 
[Optocoupler input protection](https://electronics.stackexchange.com/questions/80597/optocoupler-input-protection),
[Common anode optocouplers protection](https://electronics.stackexchange.com/questions/293715/common-anode-optocouplers-protection)

The first input stage is used as reference.
So, no ESD protection, because it is not needed. And D4 is for reverse polarity protection.

Calculations.

Optocoupler input

Input voltage (Vin / Ext_Input_1): 3.3V - 24V

R3 = 1k ohm

optocoupler = TPC817S1B RAG

V(forward) = 1.2 - 1.4 V

V(CE saturated) = 0.1 - 0.2 V (when I(collector) = 2 mA)

24 - 1.2 = 22.8 V / 1k ohm = 22.8 mA

24 - 1.4 = 22.6 V / 1k ohm = 22.6 mA

3.3 - 1.2 = 2.1 V / 1k ohm = 2.1 mA

3.3 - 1.4 = 1.9 V / 1 k ohm = 1.9 mA

Optocoupler output

R10 = 1 k ohm

3.3 V - 0.1 V = 3.2 V / 1 k ohm = 3.2 mA

3.3 V - 0.2 V = 3.1 V / 1 k ohm = 3.1 mA
# device/#

    device/$DEVICE_ID/status
        {
            "online": true,
            "firmware": "dlock-esp", "gitHash": ..., "buildTimestamp": "2019-12-",
            "applications": [
                "dlock/$DOOR_ID",
            ]
        }

    device/$DEVICE_ID/status
        {"online": false}

    device/$DEVICE_ID/log
        $LOG
    
    device/$DEVICE_ID/ota
        {"url": "https://.."}

    device/$DEVICE_ID/config
        {
            "mqttClientId": "",
            "mqttKeepAlive": "",
            "mqttUsername": "",
            "mqttPassword": "",
            "mqttHost": "",
        }
    
    device/$DEVICE_ID/apps
        {
            "door_ids": ["slurpen-høyre", "slurpen-venstre"],
            "doors": [
                {
                    "id": "slurpen-høyre",
                    "port": "a",
                    "invertedPins": []
                }
            ]
        }

# dlock/#

    // changes frequently
    dlock/$DOOR_ID/status
        {
            "locked": true,
            "lockedSince": true,
            "closed": true,
            "closedSince": true,
        }
    
    // Static info
    dlock/$DOOR_ID/info
        {
            "device": "$DEVICE_ID",
            "hasClosedInput": true,
            "hasForceOpenInput": false,
        }

    dlock/$DOOR_ID/command
           {"state": "lock", "seconds": 123}
           {"state": "lock", "permanent": true}
           {"state": "unlock", "permanent": true}
           {"state": "unlock", "seconds": 321}

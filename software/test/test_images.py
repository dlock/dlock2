from dlock.central import *
from pathlib import Path
import time

def test_parse_timestamp():
    ts = "Sep 10 2019 12:37:19"
    dt = parse_image_timestamp(ts)
    assert(dt.year == 2019)
    assert(dt.month == 9)
    assert(dt.day == 10)
    assert(dt.hour == 12)
    assert(dt.minute == 37)
    assert(dt.second == 19)

    assert(format_image_timestamp(dt) == ts)

def test_find_images(tmpdir):
    basedir = Path(tmpdir)
    images = OtaImages(basedir)

    assert(images._needs_reload() is None)

    time.sleep(0.1) # sleeps are required because the FS's resolution is quite bad
    (basedir / "foo.bin").open("w").close()

    assert(images._needs_reload() is not None)
    assert(len(images.images) == 1)

    assert((basedir / "foo.meta").is_file())

    assert(images._needs_reload() is None)

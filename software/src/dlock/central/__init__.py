import argparse
import csv
import json
import logging
import os
import os.path
import socket
from datetime import datetime
from functools import total_ordering
from pathlib import Path
from typing import Optional, List

import paho.mqtt.client

logger = logging.getLogger(__name__)


class DeviceConfig:
    def __init__(self, mac, url):
        self.mac = mac
        self.url = url


class Devices:
    def __init__(self, path: Path):
        self.path = path
        self.mtime = 0
        self.devices = []
        self.load()

    def load(self):
        try:
            ret = self._load()
            if ret is not None:
                self.mtime, self.devices = ret
        except Exception as e:
            logger.warning("Unable to read devices file: {}".format(e), exc_info=True)

    def _load(self):
        if not self.path.is_file():
            return

        mtime = self.path.stat().st_mtime_ns
        if self.mtime == mtime:
            return

        logger.info("Loading devices file")
        devices = []

        with self.path.open("r") as f:
            reader = csv.DictReader(f)
            for d in reader:
                devices.append(DeviceConfig(d["mac"], d["url"]))

        logger.info("Loaded {} devices".format(len(devices)))
        return mtime, devices

    def find_by_mac(self, mac):
        for d in self.devices:
            if d.mac == mac:
                return d


@total_ordering
class OtaImage:
    def __init__(self, path, project, sequence, timestamp, git_id, git_dirty):
        self.path = path
        self.project = project
        self.sequence = sequence
        self.timestamp = timestamp
        self.git_id = git_id
        self.git_dirty = git_dirty

    def __eq__(self, other):
        return self.path == other.path

    def __lt__(self, other):
        return self.timestamp < other.timestamp


def parse_image_timestamp(ts):
    return datetime.strptime(ts, "%b %d %Y %H:%M:%S")


def format_image_timestamp(dt):
    return dt.strftime("%b %d %Y %H:%M:%S")


class OtaImages:
    def __init__(self, image_dir: Path, meta_dir: Path):
        self.image_dir = image_dir
        self.meta_dir = meta_dir
        self._images: List[OtaImage] = []
        self.mtime = 0
        self.update_cache()

    def update_cache(self):
        self.meta_dir.mkdir(parents=True, exist_ok=True)

        try:
            logger.info("Updating OTA image cache")
            ret = self._update_cache()
            if ret is not None:
                self.mtime, self._images = ret
                logger.info("Loaded {} images".format(len(self._images)))
                for img in self._images:
                    logger.info(
                        f" #{img.sequence}: {img.path}, git={img.git_id}, dirty={img.git_dirty}, timestamp={img.timestamp}")

        # for i in self._images:
        #     logger.info(" image {}: {}".format(i.path, i.timestamp))
        except Exception as e:
            logger.warning("Unable to discover images: {}".format(e), exc_info=True)

    def _needs_reload(self):
        mtime = self.image_dir.stat().st_mtime_ns
        return mtime if self.mtime != mtime else None

    def _update_cache(self):
        mtime = self._needs_reload()
        if mtime is None:
            return

        images = []

        for path in self.image_dir.iterdir():
            root, ext = os.path.splitext(path.name)
            if ext != ".bin":
                continue

            git_dirty = root.endswith("-dirty")
            root = root if not git_dirty else root[0:-6]
            idx = root.rfind("-")

            if idx == -1:
                return

            project = root[0:idx]
            git_id = root[idx + 1:]

            image = self._on_image(path, project, git_id, git_dirty)
            if image is not None:
                images.append(image)

        images = sorted(images)
        sequence = 0
        for i in images:
            i.sequence = sequence
            sequence = sequence + 1

        return mtime, images

    def _on_image(self, image_path, project, git_id, git_dirty):
        def default(obj):
            if isinstance(obj, datetime):
                return obj.isoformat()
            raise TypeError("Type {} not serializable".format(type(obj)))

        # timestamp_path = image_path.parent / (image_path.name[0:-3] + ".timestamp")

        meta_path = self.meta_dir / (image_path.name[0:-4] + ".meta")

        if meta_path.is_file():
            with meta_path.open("r") as f:
                meta = json.load(f)
            project = meta["project"]
            sequence = None  # meta["sequence"]
            timestamp = datetime.fromisoformat(meta["timestamp"])
            git_id = meta["gitId"]
        else:
            sequence = None
            mtime = image_path.stat().st_mtime_ns
            timestamp = datetime.fromtimestamp(int(mtime / 1e9))
            meta = {
                "project": project,
                "timestamp": timestamp,
                "gitId": git_id
            }

            logger.info("Writing meta file {}".format(meta_path))
            with meta_path.open("w") as f:
                json.dump(meta, f, sort_keys=True, indent=4, default=default)
                f.writelines("\n")

        return OtaImage(image_path, project, sequence, timestamp, git_id, git_dirty)

    @property
    def images(self):
        self.update_cache()

        return self._images

    def newest(self) -> Optional[OtaImage]:
        images = self.images

        if len(images) == 0:
            return None

        return images[-1]

    def find_by_git_it(self, git_id: str) -> Optional[OtaImage]:
        git_dirty = git_id.endswith("-dirty")

        if git_dirty:
            git_id = git_id[0:-6]

        for i in self.images:
            if i.git_id == git_id and i.git_dirty == git_dirty:
                return i

from typing import List, Optional

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean, BLOB, func
from sqlalchemy.orm import relationship, Session

from .model_utils import Base, MutableMixin, ImmutableMixin

__all__ = [
    "FirmwareDao",
    "Application",
    "Image",
    "ImageFile",
]


class Application(Base, MutableMixin):
    __tablename__ = "firmware_application"

    name = Column(String)
    images = relationship("Image", back_populates="application")


class Image(Base, ImmutableMixin):
    __tablename__ = "firmware_image"

    application_id = Column("application_id", ForeignKey("firmware_application.id"))
    application = relationship("Application", back_populates="images")

    is_release = Column(Boolean)
    version = Column(String)
    build_timestamp = Column(DateTime)
    git_hash = Column(String)
    git_hash_seq = Column(Integer)
    git_dirty = Column(Boolean)

    files = relationship("ImageFile", back_populates="image")

    @property
    def description(self):
        if self.is_release:
            s = self.version

            if self.git_hash:
                s += f" ({self.git_hash})"

            return s

        s = self.git_hash

        if self.git_dirty:
            s += "-dirty"

            if self.git_hash_seq:
                s += f"-{self.git_hash_seq}"

        if self.build_timestamp:
            s += f" ({self.build_timestamp})"

        return s


class ImageFile(Base, ImmutableMixin):
    __tablename__ = "firmware_image_file"

    image_id = Column("image_id", ForeignKey("firmware_image.id"))
    image = relationship("Image", back_populates="files")
    type = Column(String)
    filename = Column(String)
    size = Column(Integer)
    md5 = Column(String)
    sha256 = Column(String)
    bytes = Column(BLOB)


class FirmwareDao:
    def __init__(self, s: Session):
        self.s = s

    def all_applications(self) -> List[Application]:
        return self.s.query(Application).all()

    def find_application(self, name) -> Optional[Application]:
        return self.s.query(Application).filter(Application.name == name).one_or_none()

    def find_mac_git_hash_seq(self, name):
        return self.s \
            .query(func.max(Image.git_hash_seq)) \
            .join(Application) \
            .filter(Application.name == name).one_or_none()

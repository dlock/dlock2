import logging
from datetime import datetime

from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker

from .device_models import *
from .firmware_models import *

logger = logging.getLogger(__name__)

_session_maker: Optional[sessionmaker] = None

__all__ = [
    "init_sqlalchemy_from_args",
    "init_sqlalchemy",
    "Tx",
]


def before_flush(obj, new: bool):
    if isinstance(obj, CtimeMixin):
        if new:
            obj.ctime = datetime.now()
    if isinstance(obj, MtimeMixin):
        obj.mtime = datetime.now()


def init_sqlalchemy_from_args(args):
    return init_sqlalchemy(args.db_username, args.db_password, args.db_host, args.db_port, args.db_database)


def init_sqlalchemy(username, password, host, port, database):
    from sqlalchemy.engine.url import URL
    u = URL(drivername="postgresql", username=username, password=password, host=host, port=port, database=database)
    logger.info("Database url: {}".format(u))
    engine = create_engine(u, echo=False)

    global _session_maker
    _session_maker = sessionmaker(bind=engine)

    # noinspection PyUnusedLocal
    @event.listens_for(_session_maker, 'before_flush')
    def receive_before_flush(session, flush_context, instances):
        # logger.info("before flush, new: {}, dirty: {}".format(len(session.new), len(session.dirty)))

        for obj in session.new:
            if session.is_modified(obj):
                before_flush(obj, True)

        for obj in session.dirty:
            if session.is_modified(obj):
                before_flush(obj, False)


class Tx:
    def __init__(self):
        self._firmware_dao = None

    def __enter__(self):
        global _session_maker
        logger.debug("tx: BEGIN")
        self.session: Session = _session_maker()
        self.device_dao = Dao(self.session)
        self.dao = self.device_dao

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            logger.debug("tx: COMMIT")
            self.session.commit()
        else:
            logger.debug("tx: ROLLBACK")
            self.session.rollback()

    @property
    def firmware_dao(self) -> FirmwareDao:
        if self._firmware_dao is None:
            self._firmware_dao = FirmwareDao(self.session)

        return self._firmware_dao

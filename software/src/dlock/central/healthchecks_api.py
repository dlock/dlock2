import json
import logging
import urllib.request
from http.client import HTTPResponse
from typing import List

logger = logging.getLogger(__name__)


class Healthcheck:
    def __init__(self, j):
        self.j = j

    def __getattr__(self, item):
        return self.j[item]


class HealthchecksApi:
    def __init__(self, base_url, api_key):
        self.base_url = base_url
        self.headers = {"X-Api-Key": api_key}

    def checks_url(self):
        return "{}/api/v1/checks/".format(self.base_url)

    def all_checks(self) -> List[Healthcheck]:
        req = urllib.request.Request(url=self.checks_url(), headers=self.headers, method="GET")
        res: HTTPResponse = urllib.request.urlopen(req)

        assert res.code == 200

        body = res.read()
        j = json.loads(body)

        return [Healthcheck(check) for check in j["checks"]]

    def create(self, body) -> Healthcheck:
        data = json.dumps(body).encode("utf-8")
        headers = {**self.headers, **{"Content-Encoding": "application/json"}}
        logger.info("req headers {}".format(headers))
        req = urllib.request.Request(url=self.checks_url(), headers=headers, method="POST", data=data)
        res: HTTPResponse = urllib.request.urlopen(req)

        assert res.code == 201

        body = res.read()
        j = json.loads(body)

        logger.info("Created Healthcheck: {}".format(j))

        return Healthcheck(j)

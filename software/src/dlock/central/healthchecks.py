import argparse
from datetime import datetime
import json
import logging
import re
import urllib.error
import urllib.request

# noinspection PyUnresolvedReferences
import dlock.central.boot
from dlock.central.healthchecks_api import HealthchecksApi
from dlock.central import services, models
from dlock.central.models import Tx
from dlock.central.device_models import *
from paho.mqtt.client import MQTTMessage

from ..utils import *

logger = logging.getLogger(__name__)


# noinspection PyBroadException
class HealthchecksProcessor:
    def __init__(self, args):
        self.args = args
        self.session_maker = models.init_sqlalchemy_from_args(args)
        self.healthchecks = HealthchecksApi(args.healthchecks_url, args.healthchecks_api_key)

        client = connect_mqtt(args, self)

        try:
            client.loop_forever()
        except KeyboardInterrupt:
            logger.warning("Got stop signal")

    # noinspection PyUnusedLocal
    def on_connect(self, client, user_data, flags, rc):
        logger.info("Connected with result code {}".format(rc))

        client.subscribe("{}/+/status".format(self.args.mqtt_prefix))

        if self.args.enable_fbp:
            client.subscribe("fbp")

    def _on_fbp(self, tx: Tx, status):
        role = status["payload"]["role"]

        m = re.match(r"^doors/(.*)", role)
        if not m:
            logger.info("Invalid role in FBP message: '{}'", role)
            return

        hostname = m.group(1)

        device = tx.dao.find_device_by_hostname(hostname)

        if device is None:
            services.create_new_device(tx, hostname=hostname)

        fake_status = {"online": True}

        self.handle_status_update(tx, device, fake_status)

    def _on_status(self, tx: Tx, mac, status):
        device = tx.dao.find_device_by_mac(mac)

        if device is None:
            services.create_new_device(tx, mac=mac)

        self.handle_status_update(tx, device, status)

    def handle_status_update(self, tx: Tx, device: Device, status):
        tags = ["auto-created"]

        if device.mac is not None:
            name = device.mac
        elif device.hostname is not None:
            tags.append("fbp")
            name = device.hostname
        else:
            raise Exception("Device is missing both mac and hostname.")

        if device.healthchecks is None:
            check = next(filter(lambda c: c.name == name, self.healthchecks.all_checks()), None)

            if check is None:
                timeout = 10 * 60  # 10 minutes
                grace = round(timeout * 0.1)
                params = {
                    "name": name,
                    "timeout": timeout,
                    "grace": grace,
                    "tags": " ".join(tags)
                }
                check = self.healthchecks.create(params)

            device.healthchecks = DeviceHealthchecks(ok_url=check.ping_url, fail_url=check.ping_url + "/fail")
            # tx.session.add(device)
            # tx.session.add(device.healthchecks)

        online = status.get("online")

        device_status = device.status
        status_dirty = False
        if device_status is None:
            device_status = DeviceStatus(device=device)
            status_dirty = True

        if online and device_status.offline:
            device_status.last_online = datetime.now()
            status_dirty = True
            tx.session.add(DeviceEvent(device=device, event="online"))

        if not online and device_status.online:
            device_status.last_offline = datetime.now()
            status_dirty = True
            tx.session.add(DeviceEvent(device=device, event="offline"))

        if status_dirty:
            tx.session.add(device_status)

        # TODO: this should happen outside of the transaction, we don't want the transaction to fail if the healthchecks
        # update failed.
        if device.healthchecks is not None:
            try:
                if online and device.healthchecks.ok_url is not None:
                    urllib.request.urlopen(device.healthchecks.ok_url)

                if not online and device.healthchecks.fail_url is not None:
                    urllib.request.urlopen(device.healthchecks.fail_url)
            except urllib.error.HTTPError as e:
                if e.code == 404:
                    logger.warning("Healthchecks device is gone: name={}, expected URL={}".format(name, e.filename))
                else:
                    raise e

        logger.info("Updated {}: {}".format(name, online))

    # noinspection PyUnusedLocal
    def on_message(self, client, user_data, msg: MQTTMessage):
        if len(msg.payload) == 0:
            logger.info("Ignoring empty message")
            return

        try:
            payload = msg.payload.decode("utf-8")
            status = json.loads(payload)
        except Exception:
            logger.warning("Unable to decode message, topic={}".format(msg.topic), exc_info=True)
            return

        try:
            m = re.match("{}/([^/]+)/status".format(self.args.mqtt_prefix), msg.topic)

            if msg.topic == "fbp":
                with Tx() as tx:
                    self._on_fbp(tx, status)
            elif m:
                mac = m[1]
                with Tx() as tx:
                    self._on_status(tx, mac, status)
            else:
                logger.warning("Unexpected topic: {}".format(msg.topic))

        except Exception:
            logger.warning("Unable to process status for {}".format(msg.topic), exc_info=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--healthchecks-url", required=True, action=EnvDefault, env="HEALTHCHECKS_URL")
    parser.add_argument("--healthchecks-api-key", required=True, action=EnvDefault, env="HEALTHCHECKS_API_KEY")
    parser.add_argument("--enable-fbp", action="store_true")
    args_add_db(parser)
    args_add_mqtt(parser)
    args = parser.parse_args()

    if args.mqtt_prefix.endswith("/"):
        args.mqtt_prefix = args.mqtt_prefix[0:-1]

    HealthchecksProcessor(args)


if __name__ == "__main__":
    main()

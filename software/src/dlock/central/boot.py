# try:
#     import googleclouddebugger
#
#     googleclouddebugger.enable()
#
# except ImportError:
#     pass

import logging

logging.basicConfig(format="%(asctime)s %(name)-50s %(levelname)-8s %(message)s", level=logging.DEBUG)

logging.getLogger("sqlalchemy").setLevel(logging.INFO)
logging.getLogger("paho.mqtt.client").setLevel(logging.INFO)

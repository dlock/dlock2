from typing import Optional

from sqlalchemy import Column, Integer, DateTime, Sequence
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

__all__ = [
    "Base",
    "IdMixin",
    "CtimeMixin",
    "MtimeMixin",
    "MutableMixin",
    "ImmutableMixin",
]

Base = declarative_base()

id_seq = Sequence("id_seq")


class IdMixin:
    id = Column(Integer, id_seq, primary_key=True)


class CtimeMixin:
    ctime = Column(DateTime)


class MtimeMixin:
    mtime = Column(DateTime)


class MutableMixin(IdMixin, CtimeMixin, MtimeMixin):
    pass


class ImmutableMixin(IdMixin, CtimeMixin):
    pass

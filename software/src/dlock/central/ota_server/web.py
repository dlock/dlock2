from datetime import datetime
import hashlib
import logging
import os
from typing import TypeVar

from flask import Flask, request

from .. import firmware_models
from ..models import *
from ... import ClientException

__all__ = ["app"]

logger = logging.getLogger(__name__)

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = os.getenv("UPLOAD_FOLDER", "/tmp")

T = TypeVar("T")


def form_param(key, required=False, default=None, parse=None) -> T:
    value = request.form.get(key, default=None)

    if value is None:
        if default is not None:
            value = default()

        if value is None and required:
            raise ClientException(f"Missing required form parameter {key}")

        return value

    if parse is not None:
        return parse(value)

    return value


@app.route("/")
def get_index():
    with Tx() as tx:
        s = "Applications: \n"
        for a in tx.firmware_dao.all_applications():
            s += f"* {a.id}: {a.name}\n"
        s += "\n"
        return s


@app.route("/upload-image", methods=["POST"])
def upload_image():
    with Tx() as tx:
        logger.info(f"files: {request.files}")
        logger.info(f"args: {request.args}")
        logger.info(f"form: {request.form}")

        app_name = form_param("application")
        build_timestamp = form_param("build_timestamp", default=datetime.now, parse=datetime.fromisoformat)
        release = form_param("release", parse=bool)
        git_hash = form_param("git_hash")

        a = tx.firmware_dao.find_application(app_name)
        if a is None:
            raise ClientException(f"No such application {app_name}")

        x = tx.firmware_dao.find_max_sequence_no(app_name)
        logger.info(f"x: {x}")
        sequence_no = (x[0] or 0) + 1

        image = firmware_models.Image(application_id=a.id, build_timestamp=build_timestamp, sequence_no=sequence_no,
                                      release=release, git_hash=git_hash)

        rows = [image]
        file_keys = ["bin", "elf", "sdkconfig"]

        for key in file_keys:
            file = request.files[key]
            filename = file.filename
            bs = file.stream.read()
            md5 = hashlib.md5()
            md5.update(bs)
            sha256 = hashlib.sha256()
            sha256.update(bs)

            image_file = firmware_models.ImageFile(image=image, type=key, filename=filename, size=len(bs),
                                                   md5=md5.hexdigest(), sha256=sha256.hexdigest(), bytes=bs)
            rows.append(image_file)

        tx.session.add_all(rows)

    return "Hello world\n"


@app.errorhandler(ClientException)
def on_client_exception(ex: ClientException):
    msg = f"Bad request: {ex.message}\n"
    logger.warning(msg)
    return msg, 400

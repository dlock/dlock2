import argparse
import json
import logging
import re
import ssl
import threading
from http.server import HTTPServer, SimpleHTTPRequestHandler
from pathlib import Path
from typing import MutableMapping, Optional

# noinspection PyUnresolvedReferences
import dlock.central.boot
import pykka
from dlock.central.tls import create_cert
from gevent.pywsgi import WSGIServer
from paho.mqtt.client import MQTTMessage

from .web import app
from .. import models, services, OtaImages
from ..models import Tx
from ...utils import *

logger = logging.getLogger(__name__)


class Topics:
    def __init__(self, mqtt_prefix):
        self.mqtt_prefix = mqtt_prefix

        self.device_re = re.compile(f"^{mqtt_prefix}/device/([^/]+)/([^/]+)")
        self.device_status_pattern = self.device_topic("+", "status")
        self.dlock_re = re.compile(f"^{mqtt_prefix}/dlock/([^/]+)")

    def device_topic(self, device_id, topic):
        return f"{self.mqtt_prefix}/device/{device_id}/{topic}"

    def upgrade_topic(self, device_id):
        return self.device_topic(device_id, "upgrade")

    def dlock_topic(self, device_id, topic):
        return f"{self.mqtt_prefix}/dlock/{device_id}/{topic}"


topics: Topics = Topics("")


class MqttMessage:
    def __init__(self, topic: str, payload):
        self.topic = topic
        self.payload = payload
        self._str = None
        self._json = None

    @property
    def payload_as_str(self) -> Optional[str]:
        if self._str is not None:
            return self._str

        try:
            self._str = self.payload.decode("utf-8")
        except UnicodeDecodeError:
            logger.warning(f"Unable to decode message as utf-8 string, topic={self.topic}", exc_info=True)

        return self._str

    @property
    def payload_as_json(self) -> Optional[dict]:
        if self._json is not None:
            return self._json

        s = self.payload_as_str
        if s is None:
            return s

        try:
            self._json = json.loads(s)
            return self._json
        except json.JSONDecodeError:
            logger.warning(f"Unable to decode message, topic={self.topic}, payload: {s}", exc_info=True)
            return None


class DeviceActor(pykka.ThreadingActor):

    def __init__(self, args, mqtt, device_id):
        super().__init__()
        self.args = args
        self.mqtt = mqtt
        self.device_id = device_id
        self.device: Optional[models.Device] = None

    def on_receive(self, msg: MqttMessage):
        m = topics.device_re.match(msg.topic)

        if not m:
            return

        if self.device is None:
            with Tx() as tx:
                device = tx.device_dao.find_device_by_mac(self.device_id)
                if device is None:
                    device = services.create_new_device(tx, mac=self.device_id)

        if m[2] == "status":
            with Tx() as tx:
                logger.info(f"{self.device_id}: Status update")
                j = msg.payload_as_json

                if j is None:
                    logger.info(f"{self.device_id}: Could not parse payload as json")
                    return

                self.on_status(tx, device, j)

    def on_status(self, tx: Tx, device: models.Device, status: dict):
        settings = services.find_firmware_settings(tx, device)

        build_timestamp = status.get("buildTimestamp")
        git_hash = status.get("gitHash")

        logger.info("build timestamp: {}, git hash: {}".format(build_timestamp, git_hash))

        # Update the current_image state of the device
        build_changed = settings.current_build_timestamp != build_timestamp or settings.current_git_hash != git_hash

        if build_changed:
            settings.current_build_timestamp = build_timestamp
            settings.current_git_hash = git_hash

            settings.current_image = services.find_image_by_git_hash(tx, git_hash)
            tx.session.add(settings)

        # Check if the device is running the currently wanted image

        current_image = settings.current_image
        if current_image is None:
            logger.debug(f"The device is running an unknown image")
        else:
            logger.info(f"Device is running {current_image.description}")

        newest_image = services.find_next_firmware_image(tx, device)

        if not newest_image:
            logger.debug("No images available")
            return

        logger.info(f"Newest image is {newest_image.description}")

        if current_image.id != newest_image.id:
            msg = f"{self.args.ota_server_url}/{newest_image.id}"
            topic = topics.upgrade_topic(self.device_id)
            logger.info(f"Sending update message to '{topic}', payload: {msg}")
            self.mqtt.publish(topic, msg)
        else:
            logger.info("Device is up-to-date")


class DeviceManagerActor(pykka.ThreadingActor):
    def __init__(self, args, mqtt):
        super().__init__()
        self.args = args
        self.mqtt = mqtt
        self.devices: MutableMapping[str, DeviceActor] = {}

    def on_receive(self, message):
        if isinstance(message, MqttMessage):
            self.on_mqtt_message(message)

    def on_stop(self):
        logger.info("DeviceManager stopping")
        for device in self.devices.values():
            device.stop()

    def on_mqtt_message(self, msg: MqttMessage):
        m = topics.device_re.match(msg.topic)
        if m:
            device_id = m[1]
            actor = self.devices.get(device_id, None)

            if actor is None:
                logger.info(f"Starting actor for {device_id}")
                actor = DeviceActor.start(self.args, self.mqtt, device_id)

                self.devices[device_id] = actor

            actor.tell(msg)
            return

        logger.warning(f"Unknown topic: {msg.topic}")


# noinspection PyBroadException
class OtaServer:
    def __init__(self, args):
        self.args = args
        self.images = OtaImages(Path(args.images), Path(args.meta))
        self.mqtt = connect_mqtt(args, self)
        self.device_manager = DeviceManagerActor.start(args, self.mqtt)

    # noinspection PyUnusedLocal
    @staticmethod
    def on_connect(mqtt, user_data, flags, rc):
        mqtt.subscribe(topics.device_status_pattern)

    # noinspection PyUnusedLocal
    def on_message(self, mqtt, user_data, msg: MQTTMessage):
        self.device_manager.tell(MqttMessage(msg.topic, msg.payload))


def handler_factory(cli_args):
    class OtaHttpHandler(SimpleHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, directory=cli_args.images, **kwargs)

    return OtaHttpHandler


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--images", default="images")
    parser.add_argument("--meta", default="images")

    parser.add_argument("--ota-server-url", action=EnvDefault, env="OTA_SERVER_URL", default="internal")
    parser.add_argument("--ota-server-cert", action=EnvDefault, env="OTA_SERVER_CERT", default="tls")
    parser.add_argument("--ota-server-port", action=EnvDefault, env="OTA_SERVER_CERT", default="0")

    args_add_db(parser)
    args_add_mqtt(parser)

    args = parser.parse_args()

    if args.ota_server_url == "internal":
        listen_addr = get_ip()

        cert = Path(args.ota_server_cert)
        key_path = cert / "local-key.pem"
        cert_path = cert / "local.pem"

        create_cert(listen_addr, key_path, cert_path)

        port = int(args.ota_server_port)
        httpd = HTTPServer((listen_addr, port), handler_factory(args))
        port = httpd.server_address[1]
        httpd.socket = ssl.wrap_socket(httpd.socket, keyfile=key_path, certfile=cert_path,
                                       server_side=True)
        httpd_thread = threading.Thread(target=httpd.serve_forever)
        httpd_thread.daemon = True
        httpd_thread.start()
        args.ota_server_url = f"https://{listen_addr}:{port}"

    models.init_sqlalchemy_from_args(args)

    if args.ota_server_url.endswith("/"):
        args.ota_server_url = args.mqtt_prefix[0:-1]

    if args.mqtt_prefix.endswith("/"):
        args.mqtt_prefix = args.mqtt_prefix[0:-1]

    global topics
    topics = Topics(args.mqtt_prefix)

    logger.info("Configuration:")
    logger.info("HTTP URL:    {}".format(args.ota_server_url))
    logger.info("MQTT prefix: {}".format(args.mqtt_prefix))

    server = OtaServer(args)
    mqtt = server.mqtt

    mqtt.loop_start()
    http_server = WSGIServer(('', 5000), app)

    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        logger.warning("Got stop signal")
        http_server.stop(0)

    mqtt.loop_stop(force=True)
    server.device_manager.stop(True)


if __name__ == "__main__":
    main()

from typing import List, Optional

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship, Session

from .model_utils import *


class Device(Base, MutableMixin):
    __tablename__ = "device"

    mac = Column(String)
    hostname = Column(String)

    healthchecks = relationship("DeviceHealthchecks", uselist=False, back_populates="device")
    status = relationship("DeviceStatus", uselist=False, back_populates="device")


class DeviceFirmwareSettings(Base, MutableMixin):
    __tablename__ = "device_firmware_settings"

    device_id = Column(Integer, ForeignKey("device.id"))
    device = relationship("Device", foreign_keys=device_id)

    current_build_timestamp = Column(String)
    current_git_hash = Column(String)
    current_image_id = Column(ForeignKey("firmware_image.id"))
    current_image = relationship("Image", foreign_keys=current_image_id)

    # One of 'specific image' or 'channel'.
    upgrade_strategy = Column(String)

    # if upgrade_strategy == 'specific image'
    specific_image_id = Column(ForeignKey("firmware_image.id"))
    specific_image = relationship("Image", foreign_keys=specific_image_id)


class DeviceHealthchecks(Base, MutableMixin):
    __tablename__ = "device_healthchecks"

    ok_url = Column(String)
    fail_url = Column(String)

    device_id = Column(Integer, ForeignKey("device.id"))
    device = relationship("Device", back_populates="healthchecks")


class DeviceStatus(Base, MutableMixin):
    __tablename__ = "device_status"

    device_id = Column("device", Integer, ForeignKey("device.id"))
    device = relationship("Device", back_populates="status")

    last_online = Column(DateTime)
    last_offline = Column(DateTime)
    last_ping = Column(DateTime)

    @property
    def online(self):
        return not self.offline

    @property
    def offline(self):
        if self.last_online is not None:
            if self.last_offline is not None:
                return self.last_offline > self.last_online
            return False

        return True


class DeviceEvent(Base, ImmutableMixin):
    __tablename__ = "device_event"

    device_id = Column("device", Integer, ForeignKey("device.id"))
    device = relationship("Device")

    event = Column(String, nullable=False)


class DeviceDao:
    def __init__(self, s: Session):
        self.s = s

    def all_devices(self) -> List[Device]:
        return self.s.query(Device).all()

    def find_device_by_mac(self, mac) -> Optional[Device]:
        return self.s.query(Device).filter(Device.mac == mac).one_or_none()

    def find_device_by_hostname(self, hostname) -> Optional[Device]:
        return self.s.query(Device).filter(Device.hostname == hostname).one_or_none()


# TODO: Replace usage with DeviceDao
class Dao(DeviceDao):
    def __init__(self, s: Session):
        super().__init__(s)

import datetime
import logging
import random
import sys
from pathlib import Path

logger = logging.getLogger(__name__)


def create_cert(cn, key_path: Path, cert_path: Path):
    from OpenSSL import crypto
    if cert_path.is_file():
        with cert_path.open("rb") as f:
            bs = f.read()
            c = crypto.load_certificate(crypto.FILETYPE_PEM, bs)
            existing_cn = c.get_subject().CN
            logger.info("Existing CN: {}".format(existing_cn))

            if existing_cn == cn:
                logger.info(f"Using old certificate, certificates CN matches: {cn}, serial: {c.get_serial_number()}")
                return

    create_cert_cryptography(cn, key_path, cert_path)

    logger.info("Wrote new SSL key: {}, public: {}".format(key_path, cert_path))


def create_cert_cryptography(cn, key_path: Path, cert_path: Path):
    from cryptography.hazmat.primitives.asymmetric import rsa
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives import serialization
    from cryptography import x509
    from cryptography.x509.oid import NameOID
    from cryptography.hazmat.primitives import hashes

    key = rsa.generate_private_key(public_exponent=65537, key_size=2048, backend=default_backend())

    key_path.parent.mkdir(parents=True, exist_ok=True)

    with key_path.open("wb") as f:
        f.write(key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        ))

    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, cn),
    ])

    ski = x509.SubjectKeyIdentifier(x509.random_serial_number().to_bytes(length=32, byteorder=sys.byteorder))
    aki = x509.AuthorityKeyIdentifier.from_issuer_subject_key_identifier(ski)

    cert = x509.CertificateBuilder() \
        .subject_name(subject) \
        .issuer_name(issuer) \
        .public_key(key.public_key()) \
        .serial_number(x509.random_serial_number()) \
        .not_valid_before(datetime.datetime.utcnow()) \
        .not_valid_after(datetime.datetime.utcnow() + datetime.timedelta(days=10)) \
        .add_extension(ski, critical=False) \
        .add_extension(aki, critical=False) \
        .add_extension(x509.BasicConstraints(True, path_length=None), critical=True) \
        .sign(key, hashes.SHA256(), default_backend())

    # .add_extension(x509.SubjectAlternativeName([x509.DNSName(cn)]), critical=False) \

    # Write our certificate out to disk.
    with cert_path.open("wb") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))


def create_cert_openssl(cn, key_path: Path, cert_path: Path):
    from OpenSSL import crypto

    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, 2048)

    issuer_name = crypto.X509Req().get_subject()
    issuer_name.CN = cn

    cert_req = crypto.X509Req()
    cert_req.get_subject().CN = cn

    cert_req.set_pubkey(key)
    # cert_req.sign(ca_key, "sha256")
    cert_req.sign(key, b"sha256")

    c = crypto.X509()
    c.set_version(2)
    c.gmtime_adj_notBefore(0)
    c.gmtime_adj_notAfter(5 * 365 * 24 * 60 * 60)
    c.set_serial_number(int(random.randint(0, sys.maxsize)))
    c.set_issuer(issuer_name)
    c.set_subject(cert_req.get_subject())
    c.set_pubkey(cert_req.get_pubkey())
    # c.sign(ca_key, "sha256")
    c.sign(key, b"sha256")

    with cert_path.open("wb") as f:
        # f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, c.get_pubkey()))
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, c))

    with key_path.open("wb") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, key))

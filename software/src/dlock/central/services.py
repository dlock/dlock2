import logging

from dlock.central.device_models import *
from dlock.central.firmware_models import *
from dlock.central.models import Tx

logger = logging.getLogger(__name__)


def create_new_device(tx, mac=None, hostname=None) -> Device:
    logger.info(f"Creating new device, mac={mac}, hostname={hostname}")

    device = Device(mac=mac, hostname=hostname)
    tx.session.add(device)
    tx.session.add(DeviceFirmwareSettings(device=device))
    return device


def find_firmware_settings(tx: Tx, device: Device) -> Optional[DeviceFirmwareSettings]:
    return tx.session. \
        query(DeviceFirmwareSettings). \
        filter(DeviceFirmwareSettings.device_id == device.id). \
        one_or_none()


def find_next_firmware_image(tx: Tx, device: Device) -> Optional[Image]:
    settings = find_firmware_settings(tx, device)

    if settings is None:
        return None

    if settings.upgrade_strategy == "specific image":
        return settings.specific_image

    return None


# This should ideally include the device so we can narrow the search to only the application's images
def find_image_by_git_hash(tx: Tx, git_id) -> Optional[Image]:
    return tx.session.query(Image).filter(Image.git_hash == git_id).one_or_none()

import argparse
import sys

from paho.mqtt import client as mqtt_client

import dlock.central


def main():
    parser = argparse.ArgumentParser()
    dlock.central.args_add_mqtt(parser)
    parser.add_argument("--cmd", required=True)
    parser.add_argument("--device")
    parser.add_argument("--duration")
    parser.add_argument("--url")
    args = parser.parse_args()

    client = dlock.central.connect_mqtt(args, None)

    if args.cmd in ["lock", "unlock"]:
        mac = device(args)

        duration = args.duration or "5"

        topic = "{}/{}/{}".format(args.mqtt_prefix, mac, args.cmd)
        print("Publishing {}: {}".format(topic, duration))
        info = client.publish(topic, duration)
        print("publish response: {}, {}".format(info.rc, mqtt_client.error_string(info.rc)))
    if args.cmd == "upgrade":
        topic = "{}/{}/{}".format(args.mqtt_prefix, device(args), args.cmd)
        info = client.publish(topic, args.url)
    else:
        print("Unknown command: {}".format(args.cmd), file=sys.stderr)


def device(args):
    mac = args.device
    if mac is None:
        print("--device is required", file=sys.stderr)
        sys.exit(1)
    return mac


if __name__ == "__main__":
    main()

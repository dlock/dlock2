import argparse
import json
import logging
import random
from pathlib import Path

from paho.mqtt.client import MQTTMessage

from dlock.simulator.dlock import Request, DlockApplication
from ..utils import *

logger = logging.getLogger(__name__)


class SimulatorConfig:
    def __init__(self, path: Path):
        self.path = path
        self.cfg = {}

        self.load()

    def load(self):
        if self.path.is_file():
            self.cfg = json.loads(self.path.read_text(encoding="utf8"))

        original = dict(self.cfg)

        if "id" not in self.cfg:
            self.cfg["id"] = f"simulator-{random.randint(1000, 9999)}"

        if original != self.cfg:
            self.save()

    def save(self):
        logger.info(f"Writing config to {self.path}")
        self.path.write_text(json.dumps(self.cfg), encoding="utf8")

    @property
    def id(self):
        return self.cfg["id"]


class Simulator:
    def __init__(self, args):
        self.config = SimulatorConfig(Path("simulator-config.json"))
        self.should_run = True
        self.args = args

        self.status_topic = self.device_topic("status")
        self.error_topic = self.device_topic("error")
        self.lock_topic = self.dlock_topic("lock")
        self.unlock_topic = self.dlock_topic("unlock")

        self.request = Request.none()
        self.app = DlockApplication()

        logger.info(f"status_topic={self.status_topic}")

        will = {
            "topic": self.status_topic,
            "payload": json.dumps({"online": "false"})
        }
        self.client = connect_mqtt(args, self, will)

    def device_topic(self, topic):
        return f"{self.args.mqtt_prefix}/device/{self.config.id}/{topic}"

    def dlock_topic(self, topic):
        return f"{self.args.mqtt_prefix}/dlock/{self.config.id}/{topic}"

    # noinspection PyUnusedLocal
    def on_connect(self, client, user_data, level, buf):
        logger.info("Connected")
        self.client.subscribe(self.lock_topic)
        self.client.subscribe(self.unlock_topic)
        # self.client.subscribe(make_topic(self.args, "lock/#"))
        status = {
            "online": "true",
            "gitHash": "abcdef",
            "buildTimestamp": "2019-01-01T12:34"
        }
        self.client.publish(self.status_topic, json.dumps(status), retain=True)

    def on_message(self, client, user_data, msg: MQTTMessage):
        logger.info(f"topic: {msg.topic}, xx '{self.unlock_topic}'")
        if msg.topic == self.unlock_topic:
            if len(msg.payload):
                try:
                    duration = int(msg.payload)
                except ValueError:
                    logger.warning("Could not decode payload")
                    self.report_error("Bad unlock payload")
                    return
            else:
                duration = 0

            self.request = Request.unlock(duration)

    def run(self):
        while self.should_run:
            self.client.loop(0.1)
            self.app.next(self.request)
            self.request = Request.none()

    def report_error(self, msg):
        self.client.publish(self.error_topic, msg)


def main():
    parser = argparse.ArgumentParser()
    args_add_mqtt(parser)
    args = parser.parse_args()

    simulator = Simulator(args)

    try:
        simulator.run()
    except KeyboardInterrupt:
        logger.warning("Got stop signal")

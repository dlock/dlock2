import logging
from datetime import datetime, timedelta
from typing import Optional

logger = logging.getLogger(__name__)


class Request:
    def __init__(self, action, duration: Optional[int] = None, upgrade_url: Optional[str] = None):
        self.action = action
        self.duration = duration
        self.upgrade_url = upgrade_url

    @classmethod
    def none(cls):
        return Request("none")

    @classmethod
    def unlock(cls, duration):
        return Request("unlock", duration=duration)


class Lock:
    def __init__(self):
        self.locked = True
        self.unlock_timeout = datetime.now()

    def next(self):
        if not self.locked and self.unlock_timeout < datetime.now():
            logger.info("Locking")
            self.locked = True

    def unlock(self, duration: int):
        self.locked = False
        if duration > 0:
            self.unlock_timeout = datetime.now() + timedelta(seconds=duration)
            logger.info(f"Unlocking, duration={duration} s, timeout={self.unlock_timeout}")
        else:
            logger.info("Unlocking, no timeout")


class DlockApplication:
    def __init__(self):
        self.lock = Lock()

    def next(self, request):
        self.lock.next()

        if request.action == "none":
            return
        elif request.action == "unlock":
            self.lock.unlock(request.duration)

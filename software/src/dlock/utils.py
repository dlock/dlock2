import argparse
import logging
import os
import socket
from typing import Optional, Any, MutableMapping

import paho.mqtt.client

__all__ = [
    "get_ip",
    "EnvDefault",
    "connect_mqtt",
    "make_topic",
    "args_add_mqtt",
    "args_add_db",
]

logger = logging.getLogger(__name__)


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        return s.getsockname()[0]
    finally:
        s.close()


def connect_mqtt(args, target, will: Optional[MutableMapping[str, Any]] = None) -> paho.mqtt.client.Client:
    logger.info(f"MQTT configuration host={args.mqtt_host}, port={args.mqtt_port}, username={args.mqtt_username}")
    logger.info(f"MQTT prefix={args.mqtt_prefix}")

    client = paho.mqtt.client.Client(client_id=args.mqtt_client_id)

    for attr in ["on_connect", "on_message"]:
        if hasattr(target, attr):
            setattr(client, attr, getattr(target, attr))

    client.enable_logger()

    if args.mqtt_username:
        client.username_pw_set(args.mqtt_username, args.mqtt_password)

    if args.mqtt_tls:
        client.tls_set()

    if will is not None:
        will_topic = will.pop("topic")
        client.will_set(will_topic, **will)

    client.connect(host=args.mqtt_host, port=args.mqtt_port, keepalive=args.mqtt_keepalive)
    return client


def make_topic(args, topic) -> str:
    return f"{args.mqtt_prefix}/{topic}"


class EnvDefault(argparse.Action):
    def __init__(self, env, required=False, default=None, **kwargs):
        default = os.environ[env] if env in os.environ else default

        if required and default:
            required = False

        super(EnvDefault, self).__init__(default=default, required=required, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)


def args_add_db(parser):
    g = parser.add_argument_group("db")
    g.add_argument("--db-username", action=EnvDefault, env="DB_USERNAME")
    g.add_argument("--db-password", action=EnvDefault, env="DB_PASSWORD")
    g.add_argument("--db-host", action=EnvDefault, env="DB_HOST", default="localhost")
    g.add_argument("--db-port", action=EnvDefault, env="DB_PORT", default="5432")
    g.add_argument("--db-database", action=EnvDefault, env="DB_DATABASE")


def args_add_mqtt(parser):
    def str2bool(v):
        if isinstance(v, bool):
            return v

        if isinstance(v, str):
            s = v.lower()

            if s in ('yes', 'true', 't', 'y', '1'):
                return True

            elif s in ('no', 'false', 'f', 'n', '0'):
                return False

        raise argparse.ArgumentTypeError('Boolean value expected.')

    g = parser.add_argument_group("mqtt")
    g.add_argument("--mqtt-host", action=EnvDefault, env="MQTT_HOST", required=True)
    g.add_argument("--mqtt-port", action=EnvDefault, env="MQTT_PORT", default="1883", type=int)
    g.add_argument("--mqtt-tls", action=EnvDefault, env="MQTT_TLS", default="no", type=str2bool)
    g.add_argument("--mqtt-username", action=EnvDefault, env="MQTT_USERNAME")
    g.add_argument("--mqtt-password", action=EnvDefault, env="MQTT_PASSWORD")
    g.add_argument("--mqtt-client-id", action=EnvDefault, env="MQTT_CLIENT_ID")
    g.add_argument("--mqtt-prefix", action=EnvDefault, env="MQTT_SERVER", default="public/dlock")
    g.add_argument("--mqtt-keepalive", action=EnvDefault, env="MQTT_KEEPALIVE", default="60", type=int)

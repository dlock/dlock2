from setuptools import setup, find_packages

setup(name="dlock-central",
      version="1.0",
      packages=find_packages("src"),
      package_dir={"": "src"},
      entry_points={
          "console_scripts": [
              "dlock-mqtt=dlock.cli.mqtt:main",
              "dlock-healthchecks-processor=dlock.central.healthchecks:main",
              "dlock-ota-processor=dlock.central.ota_server:main",
              "dlock-simulator=dlock.simulator:main",
          ]
      },
      install_requires=[
          "paho-mqtt",
          "psycopg2",
          "pyOpenSSL",
          # "google-python-cloud-debugger",
          "sqlalchemy",
          "flask",
          "gevent",
          "pykka",
      ], extras_require={
        "development": [
            # "pytest", "pytest-pep8", "pytest-cov",
            "tox",
        ]})

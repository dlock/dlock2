DROP TABLE IF EXISTS device_firmware_upgrade_attempt;
DROP TABLE IF EXISTS device_firmware_settings;
DROP TABLE IF EXISTS firmware_image_file;
DROP TABLE IF EXISTS firmware_image;
DROP TABLE IF EXISTS firmware_application;

CREATE TABLE firmware_application
(
    id    BIGINT PRIMARY KEY,
    ctime TIMESTAMPTZ NOT NULL,
    mtime TIMESTAMPTZ NOT NULL,

    name  TEXT        NOT NULL UNIQUE
);
ALTER TABLE firmware_application
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON firmware_application TO "dlock-ota";

CREATE TABLE firmware_image
(
    id              BIGINT PRIMARY KEY,
    ctime           TIMESTAMPTZ NOT NULL,

    application_id  BIGINT      NOT NULL REFERENCES firmware_application,
    is_release      BOOLEAN,
    version         TEXT,     -- required if release_version
    build_timestamp TIMESTAMPTZ,
    git_hash        CHAR(40), -- required if not release_version else optional
    git_hash_seq    INTEGER,
    git_dirty       BOOLEAN
);
ALTER TABLE firmware_image
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON firmware_image TO "dlock-ota";

CREATE TABLE firmware_image_file
(
    id       BIGINT PRIMARY KEY,
    ctime    TIMESTAMPTZ NOT NULL,

    image_id BIGINT      NOT NULL REFERENCES firmware_image,
    type     TEXT        NOT NULL,
    filename TEXT        NOT NULL,
    size     TEXT        NOT NULL,
    md5      TEXT        NOT NULL,
    sha256   TEXT        NOT NULL,
    bytes    BYTEA       NOT NULL
);
ALTER TABLE firmware_image_file
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON device TO "dlock-ota";

CREATE TABLE device_firmware_settings
(
    id                      BIGINT PRIMARY KEY,
    ctime                   TIMESTAMPTZ NOT NULL,
    mtime                   TIMESTAMPTZ NOT NULL,

    device_id               BIGINT REFERENCES device,
    application_id          BIGINT REFERENCES firmware_application,

    current_git_hash        TEXT,
    current_build_timestamp TEXT,

    current_image_id        BIGINT REFERENCES firmware_image,

    upgrade_strategy        TEXT, -- 'specific image', 'channel'
    specific_image_id       BIGINT REFERENCES firmware_image

    -- TODO: channel fields
);

GRANT SELECT, INSERT, UPDATE ON device_firmware_settings TO "dlock-ota";

INSERT INTO device_firmware_settings(id, ctime, mtime, device_id)
SELECT nextval('id_seq'), current_timestamp, current_timestamp, id
FROM device;

CREATE TABLE device_firmware_upgrade_attempt
(
    id       BIGINT PRIMARY KEY,
    ctime    TIMESTAMPTZ NOT NULL,

    image_id BIGINT      NOT NULL REFERENCES firmware_image
);

GRANT SELECT, INSERT, UPDATE ON device_firmware_upgrade_attempt TO "dlock-ota";

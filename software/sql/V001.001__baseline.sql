DROP TABLE IF EXISTS device_event;
DROP TABLE IF EXISTS device_status;
DROP TABLE IF EXISTS device_healthchecks;
DROP TABLE IF EXISTS device;
DROP SEQUENCE IF EXISTS id_seq;

CREATE SEQUENCE id_seq;
GRANT USAGE ON SEQUENCE id_seq TO PUBLIC;

CREATE TABLE device
(
    id    BIGINT PRIMARY KEY,
    ctime TIMESTAMPTZ NOT NULL,
    mtime TIMESTAMPTZ NOT NULL,

    mac   TEXT        NOT NULL
);
ALTER TABLE device
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON device TO "dlock-healthchecks";

CREATE TABLE device_healthchecks
(
    id       BIGINT PRIMARY KEY,
    ctime    TIMESTAMPTZ NOT NULL,
    mtime    TIMESTAMPTZ NOT NULL,

    device   BIGINT      NOT NULL REFERENCES device (id),
    ok_url   TEXT        NOT NULL,
    fail_url TEXT        NOT NULL
);
ALTER TABLE device_healthchecks
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON device_healthchecks TO "dlock-healthchecks";

CREATE TABLE device_status
(
    id           BIGINT PRIMARY KEY,
    ctime        TIMESTAMPTZ NOT NULL,
    mtime        TIMESTAMPTZ NOT NULL,

    device       BIGINT      NOT NULL REFERENCES device (id),
    last_online  TIMESTAMPTZ,
    last_offline TIMESTAMPTZ,
    last_ping    TIMESTAMPTZ
);
ALTER TABLE device_status
    OWNER TO dlock;

GRANT SELECT, INSERT, UPDATE ON device_status TO "dlock-healthchecks";

CREATE TABLE device_event
(
    id     BIGINT PRIMARY KEY,
    ctime  TIMESTAMPTZ NOT NULL,

    device BIGINT      NOT NULL REFERENCES device (id),
    event  TEXT        NOT NULL
);
ALTER TABLE device_event
    OWNER TO dlock;

GRANT SELECT, INSERT ON device_event TO "dlock-healthchecks";

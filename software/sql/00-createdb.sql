REVOKE USAGE ON SCHEMA public FROM "dlock-healthchecks";
REVOKE USAGE ON SCHEMA public FROM "dlock-ota";
REVOKE USAGE ON SCHEMA public FROM "dlock-flyway";
REVOKE ALL PRIVILEGES ON DATABASE dlock FROM "dlock-healthchecks";
REVOKE ALL PRIVILEGES ON DATABASE dlock FROM "dlock-ota";
REVOKE ALL PRIVILEGES ON DATABASE dlock FROM "dlock-flyway";

DROP USER IF EXISTS "dlock-healthchecks";
DROP USER IF EXISTS "dlock-ota";
DROP USER IF EXISTS "dlock-flyway";

DROP DATABASE IF EXISTS "dlock";
DROP USER IF EXISTS "dlock";

--

CREATE USER "dlock" NOLOGIN;

CREATE USER "dlock-flyway" ENCRYPTED PASSWORD 'dlock-flyway' IN ROLE "dlock";
CREATE USER "dlock-ota" ENCRYPTED PASSWORD 'dlock-ota';
CREATE USER "dlock-healthchecks" ENCRYPTED PASSWORD 'dlock-healthchecks';

CREATE DATABASE dlock OWNER dlock;

GRANT CREATE ON DATABASE dlock TO "dlock-flyway";

ALTER TABLE device
    DROP COLUMN IF EXISTS hostname;

ALTER TABLE device
    ADD COLUMN hostname text,
    ALTER COLUMN mac DROP NOT NULL;

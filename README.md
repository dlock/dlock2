# Goals

* This is a development board. Should be safe to handle and easy to
  use.

## Non-goals

* Low-power operation

# Datasheets

* https://www.silabs.com/documents/public/data-sheets/cp2102n-datasheet.pdf
* https://www.silabs.com/documents/public/application-notes/an721.pdf

# Some notes on PCB performance: 
A measurement on the fly-back voltage coming from the relay when it closes shows that there is an 18 V
peak apprearing on the node between the relay and the DMN2041L transistor (named RELAY_n in the schematic for 
rev 2.1). See picture Flyback-voltage-on-relay.png. 


# Workaround for missing part  1462042-8
For rev 2.1 the relay  1462042-8 was not in stock, so instead 1462039-4 was chosen as a replacement. 
The replacement does not have the same pinout, but requires a bodge wire to make it work. 
See the piture named rev-2.1-relay-fix.jpg for documentation on how it was done. 
For the board that will be set up as a test fixture for the door in the office in Bitraf, 
only output 8 has been fixed. It has been panited red to indicate this. 



# Baseboard

# Doorlock board

Rev 1.

- Lock requirements / overall design
- Initial schematic
- Initial layout

# Firmware

## M1. dlock can run on standard ESP32

- Implement MQTT using ESP-IDF
- Test on ESP32 device

## M2. dlock can run on custom PCB

- dlock HAL implement with I/O expander
- Make dlock into an ESP-IDF component

## M3. Over-The-Air updates

Torfinn.

#include <doorsystem_json.hpp>
#include <doorsystem.hpp>
#include <fstream>
#include <vector>
#include <iostream>
#include <experimental/optional>

namespace ds = dlock::doorsystem;
using json = ds::json;
using namespace std;

template<typename T>
using optional = experimental::optional<T>;

struct Stimuli {
    optional<int> add_time;
    optional<ds::Request> request;
};

vector<Stimuli> stimulis;

bool read_input(int argc, char **argv) {
    string input;

    ifstream ifstream{};

    if (argc == 2) {
        auto filename = argv[1];
        ifstream.open(filename, ifstream::in);
        if (ifstream.bad()) {
            cerr << "Could not open " << filename << endl;
            return false;
        }
    }

    istream &is = ifstream.is_open() ? ifstream : cin;

    while (!is.eof()) {
        string line;
        getline(is, line);
        input.append(line);
        input.append("\n");
    }

    auto root = cJSON_Parse(input.c_str());

    cJSON *item;
    cJSON_ArrayForEach(item, root) {
        json j(item, false);

        Stimuli stimuli{};
        auto add_time = j.get_int("add_time");
        if (add_time) {
            stimuli.add_time = add_time;
        }

        if (j.has_item("request")) {
            auto request = j.get_obj("request");
            auto action = ds::action_from_string(request.get("action"));
            auto duration = request.get_float("duration");
            auto upgrade_url = request.get_str("upgradeUrl");
            stimuli.request = ds::Request{action, duration, upgrade_url};
        }

        stimulis.push_back(stimuli);
    }

    cJSON_Delete(root);

    return true;
}

int main(int argc, char **argv) {
    ds::Config config{};
    ds::Lock lock{ds::Lock::Locked, 0, 0, ds::Lock::Unknown};
    ds::Opener opener{ds::Opener::Inactive, 0, 0};
    ds::DoorSensor door_sensor{true, 0};
    ds::State state{lock, opener, door_sensor, false};

    if (!read_input(argc, argv)) {
        return 1;
    }

    {
        auto j = to_json(state);
        cout << "state: " << j.print().get() << endl;
    }

    float now = 0;
    ds::InputState is;
    for (auto &stimuli : stimulis) {
        auto add_time = stimuli.add_time ? *stimuli.add_time : 1;

        for (int i = 0; i < add_time; i++) {
            is.current_time = now;

            if (i == add_time - 1) {
                cout << "Applying request" << endl;
                if (stimuli.request) {
                    is.request = *stimuli.request;
                    cout << "request: action=" << to_string(is.request.action);
                    if (is.request.action == dlock::doorsystem::Request::Upgrade) {
                        cout << ", upgradeUrl=" << is.request.upgrade_url;
                    } else if (is.request.action == dlock::doorsystem::Request::None) {
                        // nothing
                    } else {
                        cout << ", duration=" << is.request.duration;
                    }
                    cout << endl;
                }
            }

            if (is.request.action == dlock::doorsystem::Request::TimedUnlock) {
                cout << "woop" << endl;
            }

            cout << "Current_time: " << is.current_time << endl;
            state = ds::next_state(config, state, is);

            cout << "state: " << to_json(state).print().get() << endl;

            is.request = ds::Request::none();
            now += 1;
        }
    }

    return 0;
}

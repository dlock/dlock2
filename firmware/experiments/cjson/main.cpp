#include <doorsystem_json.hpp>

namespace ds = dlock::doorsystem;
using json = ds::json;

const char *TAG = "main";

int main()
{
    if (true) {
        ds::Inputs c{};
        json j = to_json(c);
        auto s = j.print();
        ESP_LOGI(TAG, "json=%s", s.get());

    }

    if (true) {
        ds::Config c{};
        json j = to_json(c);
        auto s = j.print();
        ESP_LOGI(TAG, "json=%s", s.get());
    }

    return 0;
}

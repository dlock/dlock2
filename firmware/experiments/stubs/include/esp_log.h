#pragma once

#include <cstdio>

#define ESP_LOGE(TAG, ...) do {printf("%-10s E ", TAG); printf(__VA_ARGS__); printf("\n"); } while(0)
#define ESP_LOGI(TAG, ...) do {printf("%-10s I ", TAG); printf(__VA_ARGS__); printf("\n"); } while(0)

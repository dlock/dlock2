
#include "dlock/doorsystem.hpp"

#include "nlohmann_json/single_include/nlohmann/json.hpp"
#include "dlock/doorsystem_json.hpp"

#include <iostream>
#include <iterator>
#include <string>

#include <stdio.h>

namespace ds = dlock::doorsystem;

struct Input {
    ds::Config config;
    ds::Inputs inputs;
    ds::State state;
};
void to_json(json& j, Input s) {
    j = json({
        { "config", s.config },
        { "inputs", s.inputs },
        { "state", s.state },
    });
}
void from_json(const json& j, Input & o) {
    o.config = j.value("config", o.config);
    j.at("inputs").get_to(o.inputs);
    j.at("state").get_to(o.state);
}

struct Output {
    ds::State state;
};
void to_json(json& j, Output s) {
    j = json({
        { "state", s.state },
    });
}
void from_json(const json& j, Output & o) {
    j.at("state").get_to(o.state);
}

Output next_state(Input in) {
    Output out;
    out.state = ds::next_state(in.config, in.state, in.inputs);
    return out;
}

int main() {
    std::istreambuf_iterator<char> begin(std::cin), end;
    std::string std_in(begin, end);

    // FIXME: parse from stdin
    Input in = json::parse(std_in);
    Output out = next_state(in);

    printf("%s", json(out).dump().c_str());

#if 0
    ds::Config c;
    const json cc = c;
    ds::Config c2 = json::parse(cc.dump());
    printf("Config: %s\n", (json(c2)).dump().c_str());

    ds::Inputs i;
    const json ii = i;
    ds::Inputs i2 = json::parse(ii.dump());
    printf("Inputs: %s\n", (json(i2)).dump().c_str());

    ds::State s;
    const json ss = s;
    ds::State s2 = json::parse(ss.dump());
    printf("States: %s\n", (json(s2)).dump().c_str());
#endif

}

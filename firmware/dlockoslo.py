
import typing
import numbers
import math
import json
import subprocess
import copy   


class Inputs():
    def __init__(self,
                 openbutton_outside : bool = False,
                 openbutton_inside : bool = False,
                 holdopen_button : bool = False,
                 bolt_present : bool = False,
                 mqtt_connected : bool = False,
                 mqtt_request : typing.Any = None,
                 current_time : float = 0) -> None:

        self.openbutton_outside = openbutton_outside
        self.openbutton_inside = openbutton_inside
        self.holdopen_button = holdopen_button
        self.mqtt_connected = mqtt_connected
        self.mqtt_request = mqtt_request
        self.current_time = current_time
        self.bolt_present = bolt_present


class TemporaryBoolean():
    def __init__(self,
        state : str,
        since : float,
        until : float = None,
        reason : str = None,
        ) -> None:
        
        self.state = state
        self.since = since
        self.until = until
        self.reason = reason

    def __repr__(self):
        return json.dumps(self.__dict__)

## XXX: nasty
class Inactive(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('Inactive', *args, **kwargs)
class Active(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('Active', *args, **kwargs)
class TemporarilyActive(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('TemporarilyActive', *args, **kwargs)

class Unlocked(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('Unlocked', *args, **kwargs)
class Locked(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('Locked', *args, **kwargs)
class TemporarilyUnlocked(TemporaryBoolean):
    def __init__(self, *args, **kwargs):
        super().__init__('TemporarilyUnlocked', *args, **kwargs)

Opener = typing.Union[Inactive,Active,TemporarilyActive]
Lock = typing.Union[Unlocked,Locked,TemporarilyUnlocked]

class States:
    def __init__(self,
                 lock : Lock = Locked(0),
                 opener : Opener = Inactive(0),
                 bolt_present :  bool = False,
                 bolt_present_updated : float = 0.0,
                 connected_light : bool = False) -> None:

        self.lock = lock
        self.opener = opener
        self.connected_light = connected_light
        self.bolt_present = bolt_present
        self.bolt_present_updated = bolt_present_updated


### Adapter code for C++

def state_to_json(state):
    ss = copy.deepcopy(state.__dict__)
    ss['door_sensor'] = {
        'bolt_sensed': ss['bolt_present'],
        'last_updated': ss['bolt_present_updated'],
    }     
    del ss['bolt_present_updated']
    del ss['bolt_present']

    return ss

def state_from_json(state):
    ss = copy.deepcopy(state)

    # Sensor
    ss['bolt_present'] = ss['door_sensor']['bolt_sensed']
    ss['bolt_present_updated'] = ss['door_sensor']['last_updated']
    del ss['door_sensor']

    # Lock
    LockT = {
        'Locked': Locked,
        'Unlocked': Unlocked,
        'TemporarilyUnlocked': TemporarilyUnlocked,
    }[ss['lock']['state']]
    ss['lock'] = LockT(since=ss['lock']['since'],
                        until=ss['lock']['until'], reason=ss['lock']['reason'])

    # Opener
    OpenerT = {
        'Inactive': Inactive,
        'Active': Active,
        'TemporarilyActive': TemporarilyActive,
    }[ss['opener']['state']]
    ss['opener'] = OpenerT(since=ss['opener']['since'],
                    until=ss['opener']['until'])

    return ss

def input_to_json(inputs):
    ss = copy.deepcopy(inputs)

    # Request
    if ss['mqtt_request']:
        action, data = ss['mqtt_request']  
        r = {}
        if action == 'unlock' and isinstance(data, bool) and data == True:
            r = { 'action': 'unlock' }
        elif action == 'unlock' and isinstance(data, int):
            r = { 'action': 'timedunlock', 'duration': data }
        # lock permanently
        elif action == 'lock' and isinstance(data, bool) and data == True:
            r = { 'action': 'lock' }
        ss['request'] = r
        del ss['mqtt_request']

    return ss

def next_state(current: States, inputs: Inputs) -> States:

    def dumper(obj):
        try:
            return obj.toJSON()
        except:
            return obj.__dict__

    # TODO: test serialization. Roundtrips and compat
    #tos = state_to_json(current)
    #froms = state_from_json(tos)
    #print('ss', current.__dict__)
    #print('ttt', froms)
    #assert current.__dict__ == froms, (current.__dict__, froms)
    
    inp = {
        'inputs': input_to_json(inputs.__dict__),
        'state': state_to_json(current),
        'config': {}
    }
    inp = json.dumps(inp, default=dumper, indent=4, sort_keys=True)
    print('in', inp)

    args = ['dlock_next_state']
    p = subprocess.run(args, input=inp.encode('utf-8'), stdout=subprocess.PIPE)
    assert p.returncode == 0, p

    out = json.loads(p.stdout)
    state = state_from_json(out['state'])
    print('out', json.dumps(state, default=dumper, indent=4, sort_keys=True))

    return States(**state)



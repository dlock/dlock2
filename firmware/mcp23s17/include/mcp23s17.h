#pragma once

#include <driver/spi_master.h>

namespace mcp23x17 {

class mcp23s17 {
public:
    int write(uint8_t reg, uint8_t value);
    int write(uint8_t reg, uint16_t value);
    int read(uint8_t reg, uint8_t &value);
    int read(uint8_t reg, uint16_t &value);

    mcp23s17(spi_device_handle_t spi, int address) : spi(spi), address(address) {}
private:
    int exchange(uint8_t* tx_buffer, size_t tx_sz, uint8_t* rx_buffer);

    spi_device_handle_t spi;
    int address;
};

namespace registers_bank_0 {
    static const uint8_t IODIRA = 0x00;
    static const uint8_t IODIRB = 0x01;
    static const uint8_t IPOLA = 0x02;
    static const uint8_t IPOLB = 0x03;
    static const uint8_t GPINTENA = 0x04;
    static const uint8_t GPINTENB = 0x05;
    static const uint8_t GPPUA = 0x0C;
    static const uint8_t GPPUB = 0x0D;
    static const uint8_t GPIOA = 0x12;
    static const uint8_t GPIOB = 0x13;
    static const uint8_t OLATA = 0x14;
    static const uint8_t OLATB = 0x15;
}

int init(char (&storage)[sizeof(mcp23s17)], mcp23s17** instance, spi_host_device_t host, int freq, int chip_select_pin, int mcp23s17_address);

} // namespace mcp23x17

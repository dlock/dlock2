#include "mcp23s17.h"
#include "esp_log.h"
#include <strings.h>
#include <new>

static auto* TAG = "MCP23S17";

namespace mcp23x17 {

int mcp23s17::exchange(uint8_t* tx_buffer, size_t tx_sz, uint8_t* rx_buffer) {
    spi_transaction_t tx = {
        .flags = 0,
        .cmd = 0,
        .addr = 0,
        .length = tx_sz * 8,
        .rxlength = rx_buffer ? tx_sz * 8 : 0,
        .user = nullptr,
        .tx_buffer = tx_buffer,
        .rx_buffer = rx_buffer,
    };

    ESP_LOGI(TAG, "tx");
    ESP_LOG_BUFFER_HEX_LEVEL(TAG, tx_buffer, tx_sz, ESP_LOG_INFO);

    auto ret = spi_device_polling_transmit(this->spi, &tx);

    if (rx_buffer) {
        ESP_LOGI(TAG, "rx");
        ESP_LOG_BUFFER_HEX_LEVEL(TAG, rx_buffer, tx_sz, ESP_LOG_INFO);
    }

    return ret;
}

int mcp23s17::write(uint8_t reg, uint16_t value)
{
    uint8_t data[] = {
        uint8_t(0x40 | (this->address << 1)),
        reg,
        uint8_t(value & 0xff),
        uint8_t((value >> 8) & 0xff),
    };
    ESP_LOGD(TAG, "tx reg=0x%02x, value=0x%04x", reg, value);
    return exchange(data, sizeof(data), 0);
}

int mcp23s17::write(uint8_t reg, uint8_t value)
{
    uint8_t data[] = {
        uint8_t(0x40 | (this->address << 1)),
        reg,
        value,
    };
    ESP_LOGD(TAG, "tx reg=0x%02x, value=0x%02x", reg, value);
    return exchange(data, sizeof(data), 0);
}

int mcp23s17::read(uint8_t reg, uint8_t &value)
{
    uint8_t tx[] = {
        uint8_t(0x40 | (this->address << 1) | 0x01),
        reg,
        0,
    };
    uint8_t rx[sizeof(tx)];

    int ret = exchange(tx, sizeof(tx), rx);
    if (!ret) {
        value = rx[2];
    }
    ESP_LOGD(TAG, "rx reg=0x%02x, value=0x%02x", reg, value);
    return ret;
}

// int mcp23s17::init(mcp23s17& instance, spi_host_device_t host, int freq, int chip_select_pin, int mcp23s17_address)
int init(char (&storage)[sizeof(mcp23s17)], mcp23s17** instance, spi_host_device_t host, int freq, int chip_select_pin, int mcp23s17_address)
{
    ESP_LOGD(TAG, "init: cs=%d, chip address=%d", chip_select_pin, mcp23s17_address);

    spi_device_interface_config_t devcfg;
    bzero(&devcfg, sizeof(devcfg));
    devcfg.clock_speed_hz = freq;
    devcfg.mode = 0; // SPI mode
    devcfg.spics_io_num = chip_select_pin;
    devcfg.queue_size = 2;

    spi_device_handle_t spi;
    int ret = spi_bus_add_device(host, &devcfg, &spi);
    if (ret) {
        return ret;
    }

    *instance = ::new(storage) mcp23s17(spi, mcp23s17_address);

    return 0;
}

} // namespace mcp23s17

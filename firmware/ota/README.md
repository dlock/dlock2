Generating a server-side certificate for HTTPS:

    openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365 -nodes

Running the HTTP server:

    openssl s_server -WWW -key key.pem -cert cert.pem -port 8070

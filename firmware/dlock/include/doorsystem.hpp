#pragma once

#include <string>
#include <utility>

namespace dlock {

namespace doorsystem {

struct Config {
    float opener_time = 10.0;
    float unlock_time_opener = 5.0;
    float sensor_update_interval = 60.0;
};

struct Request {
    enum Action {
        None,
        Lock,
        Unlock,
        TimedUnlock,
        Upgrade,
    };
    Action action = None;
    float duration = 0.0f;
    std::string upgrade_url{};

    Request(Action _action, float _duration, std::string upgrade_url) :
        action(_action), duration(_duration), upgrade_url(std::move(upgrade_url)) {}

    static Request upgrade(const std::string &upgrade_url)
    {
        return {Action::Upgrade, 0, upgrade_url};
    }

    static Request none()
    {
        return {Action::None, 0, {}};
    }

    static Request lock()
    {
        return {Action::Lock, 0, {}};
    }

    static Request unlock()
    {
        return {Action::Unlock, 0, {}};
    }

    static Request timed_unlock(float duration)
    {
        return {Action::TimedUnlock, duration, {}};
    }
};

struct InputPorts {
    bool openbutton_outside;
    bool openbutton_inside;
    bool holdopen_button;
    bool bolt_present;

    InputPorts() : openbutton_outside(false), openbutton_inside(false),
                   holdopen_button(false), bolt_present(false) {}

    InputPorts(bool openbutton_outside, bool openbutton_inside, bool holdopen_button, bool bolt_present) :
        openbutton_outside(openbutton_outside), openbutton_inside(openbutton_inside), holdopen_button(holdopen_button),
        bolt_present(bolt_present)  {}
};

struct InputState {
    InputPorts input_ports;
    float current_time;
    Request request;
    bool mqtt_connected;

    InputState() : input_ports(), current_time(0.0f), request(Request::none()), mqtt_connected(false) {}

    InputState(InputPorts input_ports, float current_time, Request request, bool mqtt_connected) :
        input_ports(input_ports), current_time(current_time), request(std::move(request)),
        mqtt_connected(mqtt_connected) {}

    InputState with_current_time(float new_current_time)
    {
        return {input_ports, new_current_time, request, mqtt_connected};
    }
};

// Lock of the door (electronic latch)
struct Lock {
    enum State {
        Unlocked,
        Locked,
        TemporarilyUnlocked,
    };
    enum Reason {
        Unknown,
        ProgramSwitch,
    };

    State state;
    float since;
    float until;
    Reason reason;

    Lock(State state, float since, float until, Reason _reason) :
        state(state), since(since), until(until), reason(_reason) {};

    static Lock unlocked(float since)
    {
        return {State::Unlocked, since, -1.0, Reason::Unknown};
    }

    static Lock temporarily_unlocked(float now, float until)
    {
        return {Lock::TemporarilyUnlocked, now, until, Reason::Unknown};
    }

    static Lock locked(float now)
    {
        return {Lock::Locked, now, -1.0, Reason::Unknown};
    }
};

// Automatic door opener (actuated arm)
struct Opener {
    enum State {
        Inactive,
        Active,
        TemporarilyActive,
    };

    State state;
    float since;
    float until;

    Opener(State _state, float _since, float _until = -1.0) : state(_state), since(_since), until(_until) {};
};

// Sensor(s) on the door
struct DoorSensor {
    bool bolt_sensed;
    float last_updated;

    DoorSensor(bool bolt_sensed, float last_updated) : bolt_sensed(bolt_sensed), last_updated(last_updated) {};
};

struct State {
    Lock lock;
    Opener opener;
    DoorSensor sensor;
    bool connected_light;

    State(Lock lock, Opener opener, DoorSensor sensor, bool connected_light) :
        lock(lock), opener(opener), sensor(sensor), connected_light(connected_light) {}
};

Lock ensure_unlocked_for_opener(Opener opener, const InputState &i, Lock lock, float duration);

State next_state(Config config, const State &current, const InputState &input_state);

} // end namespace doorsystem
} // end namespace dlock

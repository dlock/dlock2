#pragma once

#include <doorsystem.hpp>

#if CONFIG_DLOCK_GPIO_STYLE_EXPANDER
#include "mcp23s17.h"
#endif

namespace dlock {
namespace esp {

void setup();
dlock::doorsystem::InputPorts get_inputs();
void set_state(dlock::doorsystem::State state);

#if CONFIG_DLOCK_GPIO_STYLE_EXPANDER
extern mcp23x17::mcp23s17 *expander;
#endif

} // namespace esp
} // namespace dlock

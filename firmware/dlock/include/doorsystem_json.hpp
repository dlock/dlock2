#pragma once

#include <doorsystem.hpp>
#include "esp_log.h"

#include <cassert>
#include <string>
#include <memory>
#include <cJSON.h>
#include <cstring>

namespace dlock {
namespace doorsystem {

namespace ds = dlock::doorsystem;

class json {
    cJSON *underlying;
    bool owning;
    constexpr static const char *TAG = "json";

public:
    json(const json &) = delete;

    json(json &&other) noexcept : underlying(other.underlying), owning(other.owning)
    {
        other.underlying = nullptr;
    }

    explicit json(cJSON *underlying, bool owning) noexcept : underlying(underlying), owning(owning) {};

    json() : underlying(cJSON_CreateObject()), owning(true)
    {
        assert(underlying != nullptr);
//        ESP_LOGI(TAG, "new underlying: %p", underlying);
    };

    ~json()
    {
        if (owning && underlying) {
//            ESP_LOGI(TAG, "del underlying: %p", underlying);
            cJSON_Delete(underlying);
            underlying = nullptr;
//        } else {
//            ESP_LOGI(TAG, "del underlying: (NOT)");
        }
    }

    void set(const char *key, json &&j)
    {
        assert(underlying != nullptr);
        assert(j.underlying != nullptr);
        cJSON_AddItemToObject(underlying, key, j.underlying);
        j.underlying = nullptr;
//        ESP_LOGI(TAG, "clr underlying: %p", underlying);
    }

    void set(const char *key, const char *value)
    {
        assert(underlying != nullptr);
        cJSON_AddStringToObject(underlying, key, value);
    }

    const char *get(const char *key) const
    {
        auto obj = cJSON_GetObjectItem(underlying, key);
        return cJSON_GetStringValue(obj);
    }

    json get_obj(const char *key) const
    {
        auto obj = cJSON_GetObjectItem(underlying, key);
        assert(cJSON_IsObject(obj));

        return json(obj, false);
    }

    bool has_item(const char *key) const
    {
        return cJSON_HasObjectItem(underlying, key);
    }

    std::string get_str() const
    {
        auto str = cJSON_GetStringValue(underlying);
        if (str) {
            return str;
        }
        return {};
    }

    std::string get_str(const char *key) const
    {
        auto obj = cJSON_GetObjectItem(underlying, key);
        auto str = cJSON_GetStringValue(obj);
        if (str) {
            return str;
        }
        return {};
    }

    float get_float(const char *key) const
    {
        auto obj = cJSON_GetObjectItem(underlying, key);

        if (obj == nullptr) {
            return 0;
        }

        if (!cJSON_IsNumber(obj)) {
            return 0;
        }

        return float(obj->valuedouble);
    }

    int get_int(const char *key) const
    {
        auto obj = cJSON_GetObjectItem(underlying, key);

        if (obj == nullptr) {
            return 0;
        }

        if (!cJSON_IsNumber(obj)) {
            return 0;
        }

        return obj->valueint;
    }

    void set(const char *key, const std::string &value)
    {
        set(key, value.c_str());
    }

    void set(const char *key, bool value)
    {
        assert(underlying != nullptr);
        cJSON_AddBoolToObject(underlying, key, value);
    }

    void set(const char *key, float value)
    {
        assert(underlying != nullptr);
        cJSON_AddNumberToObject(underlying, key, value);
    }

    void set(const char *key, double value)
    {
        assert(underlying != nullptr);
        cJSON_AddNumberToObject(underlying, key, value);
    }

    json at(int index)
    {
        return json(cJSON_GetArrayItem(underlying, index), false);
    }

    std::unique_ptr<char, decltype(&::free)> print()
    {
        assert(underlying != nullptr);
        return {cJSON_PrintUnformatted(underlying), &::free};
    }

//    class ArrayIterator {
//        cJSON *underlying;
//        int pos;
//    public:
//        using iterator_category = std::forward_iterator_tag;
//        using difference_type = int;
//        using value_type = json;
//        using pointer = const json *;
//        using reference = const json &;
//
//        ArrayIterator(cJSON *underlying, int pos) : underlying(underlying), pos(pos) {};
//
//        void operator++()
//        {
//            pos++;
//        }
//
//        json operator*()
//        {
//            return json(cJSON_GetArrayItem(underlying, pos));
//        }
//
//        bool operator!=(const ArrayIterator &other) const
//        {
//            return pos != other.pos;
//        }
//    };
//
//    class AsArray {
//        cJSON *underlying;
//        int sz;
//
//    public:
//        AsArray(cJSON *underlying, int sz) : underlying(underlying), sz(sz) {}
//
//        ArrayIterator begin()
//        {
//            return {underlying, 0};
//        }
//
//        ArrayIterator end()
//        {
//            return {underlying, cJSON_GetArraySize(underlying)};
//        }
//    };
//
//    AsArray as_array()
//    {
//        assert(cJSON_IsArray(underlying));
//        return {underlying, cJSON_GetArraySize(underlying)};
//    }
};

const char *to_string(Lock::State state)
{
    switch (state) {
        case Lock::Unlocked: return "Unlocked";
        case Lock::Locked: return "Locked";
        case Lock::TemporarilyUnlocked: return "TemporarilyUnlocked";
    }

    ESP_LOGE("dlock-json", "Unknown Lock::State");
    assert(false);
    return "";
}

const char *to_string(Lock::Reason reason)
{
    switch (reason) {
        case Lock::Reason::Unknown: return "Unknown";
        case Lock::Reason::ProgramSwitch: return "ProgramSwitch";
    }

    ESP_LOGE("dlock-json", "Unknown Lock::Reason");
    assert(false);
    return "";
}

const char *to_string(Request::Action action)
{
    switch (action) {
        case Request::Action::None: return "None";
        case Request::Action::Lock: return "Lock";
        case Request::Action::Unlock: return "Unlock";
        case Request::Action::TimedUnlock: return "TimedUnlock";
        case Request::Action::Upgrade: return "Upgrade";
    }

    ESP_LOGE("dlock-json", "Unknown Request::Action");
    assert(false);
    return "";
}

Request::Action action_from_string(const char *str)
{
    if (std::strcmp("None", str) == 0) {
        return Request::Action::None;
    } else if (std::strcmp("Lock", str) == 0) {
        return Request::Action::Lock;
    } else if (std::strcmp("Unlock", str) == 0) {
        return Request::Action::Unlock;
    } else if (std::strcmp("TimedUnlock", str) == 0) {
        return Request::Action::TimedUnlock;
    } else if (std::strcmp("Upgrade", str) == 0) {
        return Request::Action::Upgrade;
    }

    return Request::Action::None; // TODO: Return unknown, the outer loop should warn on unknown commands
}

const char *to_string(Opener::State state)
{
    switch (state) {
        case Opener::State::Inactive: return "Inactive";
        case Opener::State::Active: return "Active";
        case Opener::State::TemporarilyActive: return "TemporarilyActive";
    }

    ESP_LOGE("dlock-json", "Unknown Opener::State");
    assert(false);
    return "";
}

// Config
json to_json(ds::Config c)
{
    json j;
    j.set("opener_time", c.opener_time);
    j.set("unlock_time_opener", c.unlock_time_opener);
    j.set("sensor_update_interval", c.sensor_update_interval);
    return j;
}

void from_json(const cJSON *j, Config &o)
{
    // if not specified, fall back to defaults
//    o.opener_time = cJSON_GetObjectItem(j, "opener_time"), o.opener_time);
//    o.unlock_time_opener = j.value("unlock_time_opener", o.unlock_time_opener);
//    o.sensor_update_interval = j.value("sensor_update_interval", o.sensor_update_interval);
}

json to_json(const ds::Request &r)
{
    json j;
    j.set("action", to_string(r.action));
    j.set("duration", r.duration);
    return j;
}

Request request_from_json(const json &j)
{
    auto action = action_from_string(j.get("action"));
    auto duration = j.get_float("duration");
    std::string upgrade_url = j.get("upgradeUrl");
    return Request{action, duration, upgrade_url};
//    j.at("action").get_to(o.action);
//    o.duration = j.value("duration", o.duration);
}

json to_json(const ds::InputState &i)
{
    json j;
    j.set("openbutton_outside", i.input_ports.openbutton_outside);
    j.set("openbutton_inside", i.input_ports.openbutton_inside);
    j.set("holdopen_button", i.input_ports.holdopen_button);
    j.set("bolt_present", i.input_ports.bolt_present);

    j.set("mqtt_connected", i.mqtt_connected);
    j.set("current_time", i.current_time);
    j.set("request", to_json(i.request));
    return j;
}

void from_json(cJSON const *j, InputPorts &o)
{
//    j.at("openbutton_outside").get_to(o.openbutton_outside);
//    j.at("openbutton_inside").get_to(o.openbutton_inside);
//    j.at("holdopen_button").get_to(o.holdopen_button);
//    j.at("bolt_present").get_to(o.bolt_present);
//    j.at("mqtt_connected").get_to(o.mqtt_connected);
//    j.at("current_time").get_to(o.current_time);
//    o.request = j.value("request", o.request);
}

// Lock
json to_json(ds::Lock s)
{
    json j;
    j.set("state", to_string(s.state));
    j.set("since", s.since);
    j.set("until", s.until);
    j.set("reason", to_string(s.reason));
    return j;
}

void from_json(cJSON const *j, Lock &o)
{
//    j.at("state").get_to(o.state);
//    if (j["since"].is_number()) {
//        j.at("since").get_to(o.since);
//    }
//    if (j["until"].is_number()) {
//        j.at("until").get_to(o.until);
//    }
//    if (j["reason"].is_string()) {
//        j.at("reason").get_to(o.reason);
//    }
}

// Opener
json to_json(ds::Opener s)
{
    json j;
    j.set("state", to_string(s.state));
    j.set("since", s.since);
    j.set("until", s.until);
    return j;
}

void from_json(cJSON const *j, Opener &o)
{
//    j.at("state").get_to(o.state);
//
//    if (j["since"].is_number()) {
//        j.at("since").get_to(o.since);
//    }
//    if (j["until"].is_number()) {
//        j.at("until").get_to(o.until);
//    }
}

json to_json(ds::DoorSensor s)
{
    json j;
    j.set("bolt_sensed", s.bolt_sensed);
    j.set("last_updated", s.last_updated);
    return j;
}

void from_json(cJSON const *j, DoorSensor &o)
{
//    j.at("bolt_sensed").get_to(o.bolt_sensed);
//    j.at("last_updated").get_to(o.last_updated);
}

// State
json to_json(ds::State s)
{
    json j;
    j.set("lock", to_json(s.lock));
    j.set("opener", to_json(s.opener));
    j.set("sensor", to_json(s.sensor));
    j.set("connected_light", s.connected_light);
    return j;
}

void from_json(cJSON const *j, State &o)
{
//    j.at("lock").get_to(o.lock);
//    j.at("opener").get_to(o.opener);
//    j.at("door_sensor").get_to(o.sensor);
//    o.connected_light = j.value("connected_light", o.connected_light);
}

} // end namespace dlock
} // end namespace doorsystem

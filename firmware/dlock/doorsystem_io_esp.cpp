#include <sdkconfig.h>
#include <doorsystem_io_esp.hpp>

#include "esp_timer.h"
#include "mcp23s17.h"

// IO backend for doorsystem using ESP GPIO

#if CONFIG_DLOCK_GPIO_STYLE_DIRECT
#include "driver/gpio.h"
static const auto dlock_input_bit_mask =
    (1ULL<<CONFIG_DLOCK_GPIO_OPENBUTTON_OUTSIDE) |
    (1ULL<<CONFIG_DLOCK_GPIO_OPENBUTTON_INSIDE) |
    (1ULL<<CONFIG_DLOCK_GPIO_HOLDOPEN) |
    (1ULL<<CONFIG_DLOCK_GPIO_BOLT_SENSOR);

static const auto dlock_output_bit_mask =
    (1ULL<<CONFIG_DLOCK_GPIO_OPENER) |
    (1ULL<<CONFIG_DLOCK_GPIO_LOCK) |
    (1ULL<<CONFIG_DLOCK_GPIO_CONNECTED);
#endif // CONFIG_DLOCK_GPIO_STYLE_DIRECT

namespace dlock {
namespace esp {

namespace ds = dlock::doorsystem;
using namespace mcp23x17;

#if CONFIG_DLOCK_GPIO_STYLE_NONE
void setup()
{
}

ds::InputPorts get_inputs()
{
    return {false, false, false, false};
}

void set_state(ds::State state)
{
}

#endif // CONFIG_DLOCK_GPIO_STYLE_NONE
#if CONFIG_DLOCK_GPIO_STYLE_DIRECT

void setup()
{
    gpio_config_t io_conf;

    // outputs
    io_conf.pin_bit_mask = dlock_output_bit_mask;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
    io_conf.pull_down_en = (gpio_pulldown_t)0;
    io_conf.pull_up_en = (gpio_pullup_t)0;
    gpio_config(&io_conf);

    // inputs
    io_conf.pin_bit_mask = dlock_input_bit_mask;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
    //io_conf.pull_up_en = 1;
    gpio_config(&io_conf);
}

ds::InputPorts get_inputs() {
    return {
            bool(gpio_get_level((gpio_num_t) CONFIG_DLOCK_GPIO_OPENBUTTON_OUTSIDE)),
            bool(gpio_get_level((gpio_num_t) CONFIG_DLOCK_GPIO_OPENBUTTON_INSIDE)),
            bool(gpio_get_level((gpio_num_t) CONFIG_DLOCK_GPIO_HOLDOPEN)),
            bool(gpio_get_level((gpio_num_t) CONFIG_DLOCK_GPIO_BOLT_SENSOR)),
    };
}

void set_state(ds::State state)
{
    gpio_set_level((gpio_num_t)CONFIG_DLOCK_GPIO_OPENER, state.opener.state != ds::Opener::Inactive);
    gpio_set_level((gpio_num_t)CONFIG_DLOCK_GPIO_LOCK, state.lock.state != ds::Lock::Locked);
    gpio_set_level((gpio_num_t)CONFIG_DLOCK_GPIO_CONNECTED, state.connected_light);
}

#endif // CONFIG_DLOCK_GPIO_STYLE_DIRECT

#if CONFIG_DLOCK_GPIO_STYLE_EXPANDER
static const int mosi = CONFIG_DLOCK_IO_EXPANDER_MOSI_PIN;
static const int miso = CONFIG_DLOCK_IO_EXPANDER_MISO_PIN;
static const int clk = CONFIG_DLOCK_IO_EXPANDER_CLK_PIN;
static const int cs = CONFIG_DLOCK_IO_EXPANDER_CS_PIN;
static const int freq = CONFIG_DLOCK_IO_EXPANDER_FREQ;

static const unsigned int MASK_OPENBUTTON_OUTSIDE = 1u << 0u;
static const unsigned int MASK_OPENBUTTON_INSIDE = 1u << 1u;
static const unsigned int MASK_HOLDOPEN = 1u << 2u;
static const unsigned int MASK_BOLT_PRESENT = 1u << 3u;

// using namespace mcp23x17::registers_bank_0;

#if CONFIG_DLOCK_IO_EXPANDER_PORT_CONFIG_A_IN_B_OUT
static const auto input_port = registers_bank_0::GPIOA;
static const auto input_iodir = registers_bank_0::IODIRA;
static const auto output_port = registers_bank_0::GPIOB;
static const auto output_iodir = registers_bank_0::IODIRB;
static const uint8_t iodir_input = 0xff;
static const uint8_t iodir_output = 0x00;
#else
#error Unsupported port config. Must be implemented
#endif

static char expander_storage[sizeof(mcp23s17)];
mcp23x17::mcp23s17 *expander;

void setup()
{
    spi_host_device_t host = HSPI_HOST;
    spi_bus_config_t bus_config {
        .mosi_io_num = mosi,
        .miso_io_num = miso,
        .sclk_io_num = clk,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 0,
        .flags = SPICOMMON_BUSFLAG_MASTER,
        .intr_flags = 0,
    };
    int dma_chan = 0;
    ESP_ERROR_CHECK(spi_bus_initialize(host, &bus_config, dma_chan));

    int chip_select = cs;
    int mcp23s17_address = 0;
    ESP_ERROR_CHECK(mcp23x17::init(expander_storage, &expander, host, freq, chip_select, mcp23s17_address));
    ESP_ERROR_CHECK(expander->write(input_iodir, iodir_input));
    ESP_ERROR_CHECK(expander->write(output_iodir, iodir_output));
}

ds::InputPorts get_inputs()
{
    uint8_t input;

    ESP_ERROR_CHECK(expander->read(input_port, input));

    return {
        bool(input & MASK_OPENBUTTON_OUTSIDE),
        bool(input & MASK_OPENBUTTON_INSIDE),
        bool(input & MASK_HOLDOPEN),
        bool(input & MASK_BOLT_PRESENT),
    };
}

void set_state(ds::State state)
{
}
#endif // CONFIG_DLOCK_GPIO_STYLE_EXPANDER

} // namespace esp
} // namespace dlock

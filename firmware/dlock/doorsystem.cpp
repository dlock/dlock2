#include "doorsystem.hpp"

namespace dlock {
namespace doorsystem {

Lock
ensure_unlocked_for_opener(Opener opener, const InputState &i, Lock lock, float duration) {
    const bool opener_active = opener.state == Opener::Active or opener.state == Opener::TemporarilyActive;
    const bool door_locked = lock.state == Lock::Locked or lock.state == Lock::TemporarilyUnlocked;

    if (opener_active and door_locked) {
        // Unlock the door to allow opener to work
        lock = { Lock::TemporarilyUnlocked, i.current_time, i.current_time+duration, Lock::Reason::Unknown };
    } else {
        // no change
    }
    return lock;
}

State
next_state(Config config, const State &current, const InputState &input_state) {
    const auto i = input_state;
    const auto p = input_state.input_ports;

    // Door lock
    Lock lock = current.lock;

    // Requests for change (normally via MQTT)
    switch (i.request.action) {
        case Request::Unlock:
            lock = Lock::unlocked(i.current_time);
            break;
        case Request::TimedUnlock:
            lock = Lock::temporarily_unlocked(i.current_time, i.current_time + i.request.duration);
            break;
        case Request::Lock:
            lock = Lock::locked(i.current_time);
            break;
        case Request::Upgrade:
        case Request::None:
            break;
    }

    // Lock again if was locked temporarily 
    if (lock.state == Lock::TemporarilyUnlocked and i.current_time >= lock.until) {
        lock = {Lock::Locked, i.current_time, -1.0, Lock::Unknown};
    }

    // Program switch, sets normally unlocked as override
    if (p.holdopen_button) {
        lock = {Lock::Unlocked, i.current_time, -1.0, Lock::ProgramSwitch};
    } else if (not p.holdopen_button and lock.state == Lock::Unlocked and lock.reason == Lock::ProgramSwitch) {
        lock = {Lock::Locked, i.current_time, -1.0, Lock::ProgramSwitch};
    }

    // Door opener buttons
    auto opener = current.opener;
    // inside button
    if ((opener.state == Opener::Inactive or opener.state == Opener::TemporarilyActive) and p.openbutton_inside) {
        opener = {Opener::TemporarilyActive, i.current_time, i.current_time + config.opener_time};
        lock = ensure_unlocked_for_opener(opener, input_state, lock, config.unlock_time_opener);
    } else if ((opener.state == Opener::Inactive or opener.state == Opener::TemporarilyActive) and
               p.openbutton_outside) {
        // outside button
        if (lock.state == Lock::Unlocked or lock.state == Lock::TemporarilyUnlocked) {
            opener = {Opener::TemporarilyActive, i.current_time, i.current_time + config.opener_time};
        } else {
            // DENY. user is outside, door is locked, have to unlock using app first
        }

        // turn off again
    } else if (opener.state == Opener::TemporarilyActive and i.current_time >= opener.until) {
        opener = {Opener::Inactive, i.current_time};
    }

    // Door presence sensor
    auto doorsensor = current.sensor;
    doorsensor.bolt_sensed = p.bolt_present; // just reflect input 1-1
    // calculate whether to update status, even though state has not changed
    if (i.current_time >= doorsensor.last_updated + config.sensor_update_interval) {
        doorsensor.last_updated = i.current_time;
    } else {
        doorsensor.last_updated = doorsensor.last_updated;
    }

    return {lock, opener, doorsensor, input_state.mqtt_connected};
}

} // end namespace doorsystem
} // end namespace dlock

#include <mcp23s17.h>
#include <doorsystem_io_esp.hpp>
#include <sdkconfig.h>

#include "esp_event.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "freertos/task.h"

static const char *TAG = "IO_TEST";

using namespace mcp23x17;
mcp23s17 *expander;
static char expander_storage[sizeof(*expander)];

int manual_gpio_config = 0;

int freq = 10*1000;
auto chip_select = gpio_num_t(CONFIG_DLOCK_IO_EXPANDER_CS_PIN);
int mcp23s17_address = 0;
spi_bus_config_t bus_config;

#if CONFIG_DLOCK_IO_EXPANDER_PORT_CONFIG_A_IN_B_OUT
static const auto input_port = registers_bank_0::GPIOA;
static const auto input_iodir = registers_bank_0::IODIRA;
static const auto output_port = registers_bank_0::GPIOB;
static const auto output_iodir = registers_bank_0::IODIRB;
#else
#error Unsupported port config. Must be implemented
#endif

void gpio_toggle_test(int cnt)
{
    auto out_pin_sel =
            (1ULL<<CONFIG_DLOCK_IO_EXPANDER_MOSI_PIN) |
            (1ULL<<CONFIG_DLOCK_IO_EXPANDER_CS_PIN) |
            (1ULL<<CONFIG_DLOCK_IO_EXPANDER_CLK_PIN) |
        0;

    {
        gpio_config_t io_conf;
        io_conf.intr_type = GPIO_INTR_DISABLE;
        io_conf.mode = GPIO_MODE_OUTPUT;
        io_conf.pin_bit_mask = out_pin_sel;
        io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
        io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
        gpio_config(&io_conf);
    }

    gpio_set_level((gpio_num_t) CONFIG_DLOCK_IO_EXPANDER_MOSI_PIN, cnt % 2);
    gpio_set_level((gpio_num_t) CONFIG_DLOCK_IO_EXPANDER_CS_PIN, cnt % 2);
    gpio_set_level((gpio_num_t) CONFIG_DLOCK_IO_EXPANDER_CLK_PIN, cnt % 2);

    auto in_pin_sel =
            (1ULL<<CONFIG_DLOCK_IO_EXPANDER_MISO_PIN) |
            (1ULL<<CONFIG_DLOCK_IO_EXPANDER_INT_PIN) |
        0;

    {
        gpio_config_t io_conf;
        io_conf.intr_type = GPIO_INTR_DISABLE;
        io_conf.mode = GPIO_MODE_INPUT;
        io_conf.pin_bit_mask = in_pin_sel;
        io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
        io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
        gpio_config(&io_conf);
    }

    int miso = gpio_get_level((gpio_num_t) CONFIG_DLOCK_IO_EXPANDER_MISO_PIN);
    int intr = gpio_get_level((gpio_num_t) CONFIG_DLOCK_IO_EXPANDER_INT_PIN);

    ESP_LOGI(TAG, "output=%d, intr=%d, miso=%d", cnt%2, intr, miso);
}

void expander_test(int cnt)
{
    using namespace mcp23x17::registers_bank_0;

    uint8_t u8; (void) u8;
    uint16_t u16; (void) u16;

    // ESP_ERROR_CHECK(expander->read(input_port, u8));
    // All outputs
    ESP_ERROR_CHECK(expander->write(output_iodir, uint8_t(0x00)));
    // All inputs
    ESP_ERROR_CHECK(expander->write(input_iodir, uint8_t(0xff)));

    for (int i = 0; i < 10; i++) {
        ESP_ERROR_CHECK(expander->read(input_port, u8));
        ESP_LOGI(TAG, "INPUT  = 0x%02x", u8);
        auto out = uint8_t((cnt % 2) ? 0xff : 0x00);
        ESP_ERROR_CHECK(expander->write(output_port, out));
        ESP_LOGI(TAG, "OUTPUT = 0x%02x", out);
    }
}

extern "C"
void app_main()
{
    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("IO_TEST", ESP_LOG_DEBUG);
    esp_log_level_set("MCP23S17", ESP_LOG_DEBUG);
    esp_log_level_set("spi_master", ESP_LOG_DEBUG);

    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "Manual config: %d", manual_gpio_config);

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    for(int i = 0; i <= GPIO_PIN_COUNT; i++) {
        ESP_LOGI(TAG, "pin #%d: input: %d, output: %d", i, GPIO_IS_VALID_GPIO(i), GPIO_IS_VALID_OUTPUT_GPIO(i));
    }

    if (manual_gpio_config) {
        spi_host_device_t host = HSPI_HOST;
        bus_config = {
            .mosi_io_num = CONFIG_DLOCK_IO_EXPANDER_MOSI_PIN,
            .miso_io_num = CONFIG_DLOCK_IO_EXPANDER_MISO_PIN,
            .sclk_io_num = CONFIG_DLOCK_IO_EXPANDER_CLK_PIN,
            .quadwp_io_num = -1,
            .quadhd_io_num = -1,
            .max_transfer_sz = 4 * 8,
            .flags = SPICOMMON_BUSFLAG_MASTER,
            .intr_flags = 0,
        };
        int dma_chan = 0;

        ESP_LOGV(TAG, "Configuring SPI bus");
        ESP_ERROR_CHECK(spi_bus_initialize(host, &bus_config, dma_chan));

        ESP_ERROR_CHECK(mcp23x17::init(expander_storage, &expander, host, freq, chip_select, mcp23s17_address));
    } else {
        dlock::esp::setup();
        expander = dlock::esp::expander;
    }
    /*
    */

    ESP_LOGI(TAG, "CS:    %d", CONFIG_DLOCK_IO_EXPANDER_CS_PIN);
    ESP_LOGI(TAG, "CLK:   %d", CONFIG_DLOCK_IO_EXPANDER_CLK_PIN);
    ESP_LOGI(TAG, "MOSI:  %d", CONFIG_DLOCK_IO_EXPANDER_MOSI_PIN);
    ESP_LOGI(TAG, "MISO:  %d", CONFIG_DLOCK_IO_EXPANDER_MISO_PIN);
    ESP_LOGI(TAG, "INT:   %d", CONFIG_DLOCK_IO_EXPANDER_INT_PIN);

    int cnt = 0;
    while (1) {
        // gpio_toggle_test(cnt);
        expander_test(cnt);

        ESP_LOGI(TAG, "cnt=%d, cnt%%2=%d", cnt, cnt%2);
        vTaskDelay(1000 / portTICK_RATE_MS);
        cnt++;
    }
}

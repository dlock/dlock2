# Getting started

    git submodule update --init

    esp-idf/install.sh   # installs stuff under ~/.espressif
    . esp-idf/export.sh

# Building some firmware

    cd dlock-esp
    export ESPBAUD=2500000
    idf.py reconfigure
    # At least configure the wifi credentials
    idf.py menuconfig
    idf.py flash monitor
    # Use ctrl-] to disconnect from the monitor

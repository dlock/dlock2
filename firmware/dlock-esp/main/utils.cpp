#include "utils.h"
#include "certificate_store.h"

#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_log.h"

#include <strings.h>

static const char *TAG = "DLOCK_UTILS";

namespace dlock {
namespace utils {

namespace ds = dlock::doorsystem;

/*
const struct pem_certificate *find_certificate(const char *file_name)
{
    for (int i = 0; i < pem_certificate_count; i++) {
        if (strcasecmp(file_name, pem_certificates[i].path) == 0) {
            return &pem_certificates[i];
        }
    }

    return nullptr;
}
*/

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:
        ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}

void do_upgrade(const ds::Request &request) {
    auto* url = request.upgrade_url.c_str();

    ESP_LOGI(TAG, "Starting OTA from %s", url);
    esp_http_client_config_t config;
    bzero(&config, sizeof(esp_http_client_config_t));
    config.url = url;
    config.use_global_ca_store = true;
    // Set to an empty string to work around that esp_https_ota_begin requires it even if use_global_ca_store is true.
    config.cert_pem = "";
    config.event_handler = _http_event_handler;
    esp_err_t ret = esp_https_ota(&config);
    if (ret == ESP_OK) {
        ESP_LOGE(TAG, "Firmware upgrade success");
        esp_restart();
    } else {
        ESP_LOGE(TAG, "Firmware upgrade failed");
    }
}

} // namespace utils
} // namespace dlock

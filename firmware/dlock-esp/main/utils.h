#pragma once

#include "doorsystem.hpp"

namespace dlock {
namespace utils {

void do_upgrade(const dlock::doorsystem::Request &request);

template<typename T, int size>
constexpr int sizeof_array(T(&)[size]) {
    return size;
}

} // namespace utils
} // namespace dlock

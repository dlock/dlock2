# This is run for every build

include(${CMAKE_CURRENT_LIST_DIR}/../git-desc.cmake)

string(TIMESTAMP TIMESTAMP)
file(WRITE ${HEADER}.in "// This file is generated
extern const char *git_id;
extern const char *build_timestamp;")
file(WRITE ${SOURCE}.in " // This file is generated
const char* git_id = \"${GIT_DESC}\";
const char* build_timestamp = \"${TIMESTAMP}\";
")
file(WRITE ${SOURCE}.in.ts "GIT_DESC=${GIT_DESC}")

configure_file(${HEADER}.in ${HEADER})

file(TIMESTAMP ${SOURCE}.ts OLD_TS)
configure_file(${SOURCE}.in.ts ${SOURCE}.ts)
file(TIMESTAMP ${SOURCE}.ts NEW_TS)

# message("NEW_TS=${NEW_TS}, OLD_TS=${OLD_TS}")
# message("GIT_DESC=${GIT_DESC}")

#if ("${GIT_DESC}" MATCHES "-dirty")
#    message("IS DIRTY")
#endif ()

if (NOT ("${NEW_TS}" STREQUAL "${OLD_TS}") OR "${GIT_DESC}" MATCHES "-dirty")
    message("Updating build info: git_desc=${GIT_DESC}, build_timestamp=${TIMESTAMP}")
    configure_file(${SOURCE}.in ${SOURCE})
endif ()

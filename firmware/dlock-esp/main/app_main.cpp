#include "doorsystem.hpp"
#include "doorsystem_io_esp.hpp"
#include "doorsystem_json.hpp"
#include "certificate_store.h"
#include "utils.h"
#include "build-info.h"

#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "protocol_examples_common.h"

#include "mqtt_client.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <cstdio>
#include <cstdint>
#include <cstddef>
#include <cstring>
#include <esp_tls.h>

static void mqtt_event_handler(void *, esp_event_base_t, int32_t, void *);

namespace ds = dlock::doorsystem;
using namespace dlock::utils;
using json = ds::json;

static const char *TAG = "dlock-app";
ds::Request upgrade_request = ds::Request::none();

bool str_endswith(const char *str, const char *suffix) {
    int suffix_len = strlen(suffix);
    int str_len = strlen(str);
    if (suffix_len > str_len) {
        return false;
    }
    const char * start = &str[str_len - suffix_len];
    return strncmp(start, suffix, str_len) == 0;
}

ds::Request
request_from_message(const char *topic, const char *data)
{
    if (str_endswith(topic, "/lock")) {
        // don't care what payload is
        return ds::Request::lock();
    } else if (str_endswith(topic, "/unlock")) {
        if (strcmp(data, "true") == 0) {
            // unlock permanently
            return ds::Request::unlock();
        } else {
            // unlock for N seconds
            const float time = strtof(data, nullptr);
            if (time == 0.0) {
                ESP_LOGI(TAG, "MQTT invalid data payload for unlock: %s", data);
            } else {
                return ds::Request::timed_unlock(time);
            }
        }
    } else if (str_endswith(topic, "/upgrade")) {
        return ds::Request::upgrade(data);
    } else {
        ESP_LOGI(TAG, "MQTT unknown topic %s", topic);
    }

    return ds::Request::none();
}

bool
fill_topic(char *buffer, size_t len, const char *port_name) {
    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);
    int chars = snprintf(buffer, len, "%s/%X:%X:%X:%X:%X:%X/%s", CONFIG_MQTT_PREFIX,
                         mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], port_name);
    return (chars > 0) && (chars < len);
}

class App {
    static const ds::Lock default_lock_state;
    static const ds::Opener default_opener_state;
    static const ds::DoorSensor default_door_sensor_state;

    esp_mqtt_client_handle_t mqtt_client{};
    bool mqtt_connected = false;
public:
    ds::Config config{};
    ds::State state{default_lock_state, default_opener_state, default_door_sensor_state, false};

    void start();
    void next(const ds::Request &request);
    void on_connected();
    void on_data(const char *, const char *);

    static void show_inputs(const ds::InputState& inputs);
    static void show_state(const ds::State& old_state, const ds::State& state);
};

const ds::Lock App::default_lock_state = {
    ds::Lock::State::Locked,
    0.0,
    0.0,
    ds::Lock::Reason::Unknown,
};

const ds::Opener App::default_opener_state {
    ds::Opener::State::Inactive,
    0,
    0,
};

const ds::DoorSensor App::default_door_sensor_state {
    false,
    0,
};

void
App::start()
{
    // IO
    dlock::esp::setup();

    ESP_LOGI(TAG, "MQTT URI=%s", CONFIG_MQTT_URI);

    // MQTT
    esp_mqtt_client_config_t mqtt_cfg{};
    mqtt_cfg.uri = CONFIG_MQTT_URI;
//    mqtt_cfg.cert_pem = (const char *)mqtt_pem;
    mqtt_cfg.keepalive = CONFIG_MQTT_KEEPALIVE;

    char lwt_topic[100];
    fill_topic(lwt_topic, sizeof_array(lwt_topic), "status");

    char lwt_msg[100];
    auto sz = snprintf(lwt_msg, sizeof_array(lwt_msg), "{\"online\":false}");

    mqtt_cfg.lwt_topic = lwt_topic;
    mqtt_cfg.lwt_msg = lwt_msg;
    mqtt_cfg.lwt_msg_len = sz;
    mqtt_cfg.lwt_retain = 1;

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client,
                        (esp_mqtt_event_id_t)ESP_EVENT_ANY_ID,
                        mqtt_event_handler, this);

    this->mqtt_client = client;

    #if CONFIG_ENABLE_MQTT
    esp_mqtt_client_start(client);
    #endif

    {
        json j = to_json(state);
        auto s = j.print();
        ESP_LOGI(TAG, "Initial state: %s", s.get());
    }
}

void
App::on_connected()
{
    // Subscribe to MQTT topics for input
    char topic[100];

    fill_topic(topic, sizeof_array(topic), "unlock");
    if (esp_mqtt_client_subscribe(mqtt_client, topic, 0) == -1) {
        goto fail;
    }
    ESP_LOGI(TAG, "sent subscribe successful: %s", topic);

    fill_topic(topic, sizeof_array(topic), "lock");
    if (esp_mqtt_client_subscribe(mqtt_client, topic, 0) == -1) {
        goto fail;
    }
    ESP_LOGI(TAG, "sent subscribe successful: %s", topic);

    fill_topic(topic, sizeof_array(topic), "upgrade");
    if (esp_mqtt_client_subscribe(mqtt_client, topic, 0) == -1) {
        goto fail;
    }
    ESP_LOGI(TAG, "sent subscribe successful: %s", topic);

    // After the the subscriptions are done, publish the online status. Don't want to publish the status and get an
    // upgrade request before we're ready!
    fill_topic(topic, sizeof_array(topic), "status");
    {
        json j;
        j.set("online", true);
        j.set("gitId", git_id);
        j.set("buildTimestamp", build_timestamp);
        auto str = j.print();
        esp_mqtt_client_publish(mqtt_client, topic, str.get(), 0, 0, 1);
    }

    mqtt_connected = true;
    return;

    fail:
    mqtt_connected = false;
}

void
App::on_data(const char *topic, const char *data)
{
    auto request = request_from_message(topic, data);
    if (request.action == ds::Request::Upgrade) {
        upgrade_request = request;
    } else if (request.action != ds::Request::None) {
        next(request);
    }
}

void
App::next(const ds::Request& request)
{
    ds::InputState i {
            dlock::esp::get_inputs(),
            float(esp_timer_get_time()) / 1000000,
            request,
            mqtt_connected,
    };

    show_inputs(i);

    // TODO: send error on MQTT
    // TODO: send islocked MQTT message
    // TODO: send boltpresent MQTT message
    // TODO: send FBP discovery message

    auto old_state = state;

    state = ds::next_state(config, state, i);

    dlock::esp::set_state(state);
    show_state(old_state, state);
}

void App::show_inputs(const ds::InputState &i)
{
    auto p = i.input_ports;
    ESP_LOGI(TAG, "Ports: outside=%d, inside=%d, hold open=%d, bolt=%d, connected=%d", p.openbutton_outside,
             p.openbutton_inside, p.holdopen_button, p.bolt_present, i.mqtt_connected);

//    {
//        json j = to_json(inputs);
//        auto s = j.print();
//        ESP_LOGI(TAG, "inputs: %s", s.get());
//    }
}

void App::show_state(const ds::State &old_state, const ds::State &state)
{
//    {
//        json j = to_json(state);
//        auto s = j.print();
//        ESP_LOGI(TAG, "new state: %s", s.get());
//    }

    if (old_state.lock.state != state.lock.state) {
        ESP_LOGI(TAG, "new state.lock.state: %s", to_string(state.lock.state));
        ESP_LOGI(TAG, "new state.lock.since: %f", state.lock.since);
        ESP_LOGI(TAG, "new state.lock.until: %f", state.lock.until);
    }
//    if (old_state.lock.since != state.lock.since) {
//        ESP_LOGI(TAG, "new state.lock.since: %f", state.lock.since);
//    }
//    if (old_state.lock.until != state.lock.until) {
//        ESP_LOGI(TAG, "new state.lock.until: %f", state.lock.until);
//    }
    if (old_state.lock.reason != state.lock.reason) {
        ESP_LOGI(TAG, "new state.lock.reason: %s", to_string(state.lock.reason));
    }

    if (old_state.opener.state != state.opener.state) {
        ESP_LOGI(TAG, "new state.opener.state: %s", to_string(state.opener.state));
        ESP_LOGI(TAG, "new state.opener.since: %f", state.opener.since);
        ESP_LOGI(TAG, "new state.opener.until: %f", state.opener.until);
    }
//    if (old_state.opener.since != state.opener.since) {
//        ESP_LOGI(TAG, "new state.opener.since: %f", state.opener.since);
//    }
//    if (old_state.opener.until != state.opener.until) {
//        ESP_LOGI(TAG, "new state.opener.until: %f", state.opener.until);
//    }

    if (old_state.sensor.bolt_sensed != state.sensor.bolt_sensed) {
        ESP_LOGI(TAG, "new state.sensor.bolt_sensed: %d", state.sensor.bolt_sensed);
    }
    if (old_state.sensor.last_updated != state.sensor.last_updated) {
        ESP_LOGI(TAG, "new state.sensor.last_updated: %f", state.sensor.last_updated);
    }

    if (old_state.connected_light != state.connected_light) {
        ESP_LOGI(TAG, "new connected_light: %d", state.connected_light);
    }
}

static esp_err_t
mqtt_handle_event(App *app, esp_mqtt_event_handle_t event)
{
    char topic[100];
    char data[100];

    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            app->on_connected();
            break;

        case MQTT_EVENT_SUBSCRIBED:
            //ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            //msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
            //ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;

        case MQTT_EVENT_DATA:
            snprintf(topic, sizeof_array(topic), "%.*s", event->topic_len, event->topic);
            snprintf(data, sizeof_array(data), "%.*s", event->data_len, event->data);
            app->on_data(topic, data);
            break;

        case MQTT_EVENT_UNSUBSCRIBED:
        case MQTT_EVENT_PUBLISHED:
        case MQTT_EVENT_ERROR:
        default:
            //ESP_LOGI(TAG, "Unhandled event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}


// C callback for esp_mqtt_client
static void
mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    App *app = (App *)handler_args;
    mqtt_handle_event(app, (esp_mqtt_event_handle_t)event_data);
}

volatile bool run = true;

extern "C"
void app_main()
{
    ESP_LOGI(TAG, "Starting dlock, git_id=%s, build_timestamp=%s", git_id, build_timestamp);

    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);
    ESP_LOGI(TAG, "MAC: %X:%X:%X:%X:%X:%X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    esp_log_level_set("DLOCK_APP", ESP_LOG_VERBOSE);
    esp_log_level_set("DLOCK_UTILS", ESP_LOG_VERBOSE);

    ESP_ERROR_CHECK(nvs_flash_init());

//    ESP_LOGI(TAG, "MQTT PEM, sz=%d\n%s", std::strlen((const char *) mqtt_pem), mqtt_pem);
//    ESP_LOGI(TAG, "OTA PEM, sz=%d\n%s", std::strlen((const char *) ota_pem), ota_pem);

    ESP_ERROR_CHECK(esp_tls_init_global_ca_store());
    auto chain = esp_tls_get_global_ca_store();
    ESP_LOGI(TAG, "Loading %d certificates from built-in store", pem_certificate_count);
    for (int i = 0; i < pem_certificate_count; i++) {
        auto &cert = pem_certificates[i];
        ESP_LOGI(TAG, "Loading %s", cert.path);
        int ret = mbedtls_x509_crt_parse(chain, (const unsigned char *) cert.pem, cert.sz + 1);
        if (ret < 0) {
            ESP_LOGE(TAG, "mbedtls_x509_crt_parse returned -0x%x, name: %s", -ret, cert.path);
        }
    }

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    #if CONFIG_ENABLE_MQTT
    ESP_ERROR_CHECK(example_connect());
    #endif

    // Setup MQTT
    App app;
    ESP_LOGI(TAG, "Free memory: %d bytes", esp_get_free_heap_size());

    app.start();
    ESP_LOGI(TAG, "MQTT start done..");

    // GPIO poll loop
    while (run) {
        vTaskDelay(1000 / portTICK_RATE_MS);

        if (upgrade_request.action == ds::Request::Upgrade) {
            ESP_LOGI(TAG, "Upgrade request received");
            do_upgrade(upgrade_request);
            upgrade_request = ds::Request::none();
        }

        app.next(ds::Request::none());
    }
}

include(${CMAKE_CURRENT_LIST_DIR}/git-desc.cmake)

message("Copying images, GIT_DESC=${GIT_DESC}")
execute_process(
    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.bin
    ${CMAKE_CURRENT_LIST_DIR}/images/${PROJECT_NAME}-${GIT_DESC}.bin

    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_CURRENT_LIST_DIR}/sdkconfig
    ${CMAKE_CURRENT_LIST_DIR}/images/${PROJECT_NAME}-${GIT_DESC}.sdkconfig

    COMMAND ${CMAKE_COMMAND} -E echo ${GIT_DESC} > ${CMAKE_CURRENT_LIST_DIR}/images/${PROJECT_NAME}-${GIT_DESC}.git

    COMMAND ${CMAKE_COMMAND} -E copy
    ${CMAKE_BINARY_DIR}/${PROJECT_NAME}.elf
    ${CMAKE_CURRENT_LIST_DIR}/images/${PROJECT_NAME}-${GIT_DESC}.elf
)

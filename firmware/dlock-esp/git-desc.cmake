execute_process(COMMAND
        "git"
        describe
        "--always"
        "--tags"
        "--dirty"
        WORKING_DIRECTORY
        "${CMAKE_CURRENT_SOURCE_DIR}"
        RESULT_VARIABLE res
        OUTPUT_VARIABLE GIT_DESC
        ERROR_QUIET
        OUTPUT_STRIP_TRAILING_WHITESPACE)
if(NOT res EQUAL 0)
    message("git describe failed")
endif()

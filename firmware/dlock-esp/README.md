# dlock firmware for ESP32 

## Configure

Setup esp-idf environment

    export IDF_PATH=$(cd $(pwd)/../esp-idf; pwd)
    virtualenv venv
    venv/bin/pip install -r $IDF_PATH/requirements.txt
    export PATH=$IDF_PATH/tools:$(pwd)/venv/bin:$PATH

Configure project

    idf.py menuconfig

## Build and Flash

    idf.py flash

## Serial monitor

    idf.py monitor -p /dev/ttyUSB0

(To exit the serial monitor, type ``Ctrl-]``.)

# Certificates

    openssl req -x509 -nodes -newkey rsa:2048 -keyout ca_key.pem -out ca_cert.pem -days 365
    openssl x509 -in certificates/local.pem -text
